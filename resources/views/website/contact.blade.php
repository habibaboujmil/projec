@extends('layouts.website')

@section('content')
    <section class="content bg-default padding padding-top padding-bottom">

        @include('website.partials.page_header')

        <div class="row">
            <div class="body col-sm-7 col-lg-8">

               

                <h2>Envoyer un message</h2>
                <form id="form-contact-us" accept-charset="UTF-8" action="{{ request()->url().'/submit' }}" method="POST">
                    {!! csrf_field() !!}

                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Nom</label>
                                <input type="text" class="form-control" name="firstname" placeholder="Ecrivez votre nom">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Prénom</label>
                                <input type="text" class="form-control" name="lastname" placeholder="Ecrivez votre Prénom">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control" placeholder=" Email">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Téléphone</label>
                                <input type="text" name="phone" class="form-control" placeholder="téléphone">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Message</label>
                                <textarea rows="5" name="content" class="form-control" placeholder=" Ecrivez votre message" style="height:200px;"></textarea>
                            </div>
                        </div>
                    </div>

                    @include('website.partials.form.captcha')

                    @include('website.partials.form.feedback')

                    <div class="row">
                        <div class="col-md-12 mb-5">
                            <button type="submit" class="btn btn-primary btn-submit"> Envoyer </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </section>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_key') }}"></script>
    <script type="text/javascript" charset="utf-8">
        $(function () {
            $('#form-contact-us').submit(function () {
                return submitForm($(this));
            });

            /**
             * Helper to submit the forms via ajax
             * @param form
             * @returns {boolean}
             */
            function submitForm($form)
            {
                var inputs = [];
                if (!FORM.validateForm($form, inputs)) {
                    return false;
                }

                FORM.sendFormToServer($form, $form.serialize());
                return false;
            }

        });
    </script>
@endsection
