@extends('layouts.website')

@section('content')
    <div class="row mt-5">
        <div class="col-lg-12">
            <h1 class="page-header">
                Bienvenue à Telec
            </h1>
        </div>
    </div>

    @if(config('app.is_preview'))
       <div class="card bg-light mt-3 " style="margin-bottom: 15px">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                    <p>Telec Engineering est un bureau d'études techniques spécialisé dans l'ingénierie électrique du bâtiment à usage d'habitation,tertiair et industriel.</p>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-default btn-block" href="http://www.be-telec.com/fr">
                        <i class="fa fa-github"></i>
                        Site officiel
                    </a>
                </div>
            </div>
        </div>
    </div>
<!--
    <div class="col-sm-14">
                <div class="card h-100">
                    <div class="card-header">
                        <h4 class="card-title text-primary">COURANTS FORT
                        </h4>
                    </div>
                    <div class="card-body">
                        <ul>
                            <li>Conception des installations électriques</li>
                            <li>Conception des ambiances lumineuses techniques et architecturale</li>
                            <li> Mise aux normes des locaux existant</li>
                            <li>Réaménagement des installations électriques existantes</li>
                            <li>Etude des éclairages de sécurité</li>
                            <li>Conception de l’armoire électrique telle que TGBT (Tableau Général Basse Tension) et armoires divisionnaires</li>
                            <li>Conception des postes de transformation privé et concessionnaire</li>
                            <li>Conception et dimensionnement des groupes électrogènes de secours et de sécurité</li>
                            <li>Conception et dimensionnement des alimentations sans interruption (UPS Onduleurs)</li>
                            <li>Conception et choix des schémas de liaisons à la terre</li>
                            <li>Conception des schémas électriques avec les notes de calcul</li>
                            <li>Réalisation des audits énergétiques des installations industrielles et tertiaires</li>
                        </ul>
                    </div>
                </div>
            </div>
        <div class="row mt-2 mb-5">
            <div class="col-sm-6">
                <div class="card h-100">
                    <div class="card-header">
                        <h4 class="card-title text-primary" >ASCENSEURS, MONTES CHARGES & ESCALATEURS </h4>
                    </div>
                    <div class="card-body">
                        <p>La conception techniques de divers ascenseurs et monte charge conformément aux normes en vigueurs tels que :
                        </p>
                        <ul>
                            <li><strong>EN 81-1 :1998:</strong>Règles de sécurité pour la construction et l’installation des ascenseurs – Partie 1 : Ascenseurs électriques.</li>
                            <li><strong>EN 81-2 :1998: </strong>Règles de sécurité pour la construction et l’installation des ascenseurs – Partie 2 : Ascenseurs hydrauliques</li>
                        
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="card h-100">
                    <div class="card-header">
                        <h4 class="card-title text-primary"> COURANTS FAIBLES </h4>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-6">
                                <ul>
                                    <li>Lot Infrastructure réseau informatique (VDI)</li>
                                    <li>Lot caméra de surveillance (CCTV)</li>
                                    <li>Lot domotique</li>
                                    <li>Lot téléphonie</li>
                                    <li>Lot interphonie</li>
                                    <li>Lot vidéophonie</li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <ul>
                                    <li>Lot détection automatique d’incendie</li>
                                    <li>Lot détection intrusion</li>
                                    <li>Lot contrôle d’accès</li>
                                    <li>Lot couloir rapide</li>
                                    <li>Lot télédistribution</li>
                                    <li>Lot sonorisation</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
            
    @endif

    

    <!-- Call to Action Section -->
 
@endsection