<footer class="pt-5 bg-primary text-white mt-auto">
   
    <div class="container-fluid p-3 bg-dark mt-3">
        {{--<p class="text-right float-right text-muted small">
            Copyright &copy; {{config('app.name') . ' ' . date('Y')}}
        </p>--}}
        <div class="text-center">
            <a class="text-muted small" href="/privacy-policy">Privacy Policy</a> |
            <a class="text-muted small" href="/terms-and-conditions">Terms and Conditions </a> |
            <a class="text-muted small" href="/faq">FAQs</a>
        </div>
    </div>
</footer>