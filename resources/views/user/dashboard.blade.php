@extends('layouts.admin')

@section('content')
    @if(config('app.is_preview'))
        <div class="well well-sm bg-gray-light">
            <div class="row">
                <ul>
                <h4><strong> <li> PRESENTATION GENERAL </li> </strong></h4>
                <p>    TELEC_CCTP est un service accessible qui met à votre disposition un outil permettant la génération, en toute simplicité,des bordereaux des prix et des CCTP complets à partir d’une bibliothèque de clauses préétablies, et ce, pour les travaux neufs et de rénovation.
                </p>
                </ul>
            </div>
        </div>           
        <div class="well well-sm bg-gray-light">
            <div class="row">
                <ul>
                    <li>
                        <h4 style="display: center "><strong>  FONCTIONNALITE</strong></h4>
                    </li>
                <div class="col-md-4">
                               
       
                    <ul>
                        <h5><strong> créer votre projet en tous simplicité: </strong></h5>
                        <li>1ère étape:</li>
                        <p>Remplir le formulaire de création d'un nouveau projet </p>
                        <li>2ème étape:</li>
                        <p>Choisir le lot désiré </p>
                        <li>3ème étape:</li>
                         <p>Vous serez dirigé vers une page qui contient le bordereaux spécifique à votre choix.
                        Sélectionner une ou plusieurs article et de les personnaliser en ligne en fonction du projet :
                        <ul>
                            <li>Compléter le descriptifs des équipements </li>
                            <li>Dupliquer un équipement </li>  
                        </ul> 
                        </p>
                        <li>4ème étape:</li>
                        <P> Consulter le texte intégral </P>
                        <li>5ème étape:</li>
                        <li>Exporter à tout moment vers PDF le bordereau correspond aux article sélectionnées et personnalisées.</li>
                        <li>Générer le bordereaux avec éstimation sous format PDF</li>
                        <li>Générer CPTP sous format PDF avec numérotation, page de garde générées et sommaire automatiquement pour la finalisation du projet</li>
                    </ul>
                </div>
            </ul>
            </div>
        </div>
    @endif

    

    
@endsection