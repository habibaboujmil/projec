<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter un type de batiment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="{{url('admin/settings/BuildingType')}}" method="post">
        {{csrf_field()}}
        <div class="form-group ">
          <label for="type">Nouveau Type </label>
          <input type="text" name="type" class="form-control" value="{{old('type')}}" required>
          @foreach($errors->get('type') as $msg)
          <li style="color: red"> {{$msg}}</li>
          @endforeach
        </div>
        <div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
        <input class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="submit" name="form-control" value="sauvgarder">
        </div>
      </form>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>




