@extends('layouts.admin')

@section('content')
  @if(session()->has('success'))

        <div class="alert alert-success">
          <p>{{session()->get('success')}}</p>
        </div>
        @endif
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">
						<span><i class="fa fa-table"></i></span>
						<span>Type des batiments</span>
					</h3>
				</div>

				<div class="box-body">

                 <div class="well well-sm well-toolbar">
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#create">
                        <span class="btn-label"><i class="fa fa-fw fa-user-plus"></i></span>Ajouter un type de batiment
                     </button>
                     @include('admin.settings.building_type.add')
                </div>
                </div>
				

					

					<table id="tbl-list" data-server="false" class="dt-table table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>N°:</th>
							<th>Type</th>
							<th>Action</th>
						</tr>
						</thead>
						<tbody>
							<tr>
							@foreach($type as $type)
							<td>{{ $loop->index+1 }}</td>
							<td>{{$type->type}}</td>
							<td>
                                <a href="{{url('admin/settings/building_type/'.$type->id)}}" class="btn btn-primary" > <i class="fa fa-table"></i></a>
                                <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#edit" data-id="{{$type->id}}" data-type="{{$type->type}}"><i class="fa fa-pencil-square-o"></i></button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete"><i class="fa fa-trash-o"></i></button>
                                @include('admin.settings.building_type.destroy')
                             </td>
                         </tr>
                         @endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@include('admin.settings.building_type.edit')
	</div>
@endsection