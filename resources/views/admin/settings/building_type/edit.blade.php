 <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" align="center"><b> Editer </b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/settings/BuildingType/'.$type->id
                    )}}" method="post">
                    <input type="hidden" name="_method" value="PUT">
                {{csrf_field()}}
            <input type="hidden" name="article_id" id="id" class="form-control">
                <div class="form-group ">
                    <label for="type">Type </label>
                    <input type="text" name="type" class="form-control"id="type">
                    @foreach($errors->get('title') as $msg)
                    <li style="color: red"> {{$msg}}</li>
                    @endforeach
                </div>
              
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
                    <input class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="submit" name="form-control" value="sauvgarder">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    
        <script type="text/javascript">
         $('#edit').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var type = button.data('type') 
      var id = button.data('id') 

      var modal = $(this)
      modal.find('.modal-body #type').val(type);
      modal.find('.modal-body #id').val(id);
    })
    </script>

@endsection