@extends('layouts.admin')

@section('content')
    @if(config('app.is_preview'))
        <div class="well well-sm bg-gray-light">
            <div class="row">
                <ul>
                <h4><strong> <li> PRESENTATION GENERAL </li> </strong></h4>
                <p>    TELEC_CCTP est un service accessible qui met à votre disposition un outil permettant la génération, en toute simplicité,des bordereaux des prix et des CCTP complets à partir d’une bibliothèque de clauses préétablies, et ce, pour les travaux neufs et de rénovation.
                </p>
                </ul>
            </div>
        </div>           
        <div class="well well-sm bg-gray-light">
            <div class="row">
                <ul>
                    <li>
                        <h4 style="display: center "><strong>  FONCTIONNALITE</strong></h4>
                    </li>
                <div class="col-md-4">
                    <ul>
                        <h5><strong>  Gestion des projets: </strong></h5>
                        <li>Modification et suppression d’un projet</li>
                        <li>Création, modification et suppression d’un lot</li>
                        <li>Gestion de la page de garde, etc...</li>
                        <li>Accéder et gérer le contenus des article préétablies sous la forme d’une bibliothèque : Modification, suppression, vérification etc... </li>
                        <li>Accéder et gérer le contenus des clauses préétablies: Modification, suppression, vérification etc...</li>
                    </ul>
                </div>

                <div class="col-md-4">
                    <ul>
                        <h5><strong>  Gestion des profils des collaborateurs: </strong></h5>
                        <li>création d’un nouveau compte </li>
                        <li>supprimer un compte existant </li>
                        <li>Editer les information d'un collaborateur</li>
                    </ul>
                </div>
                 <div class="col-md-4">
                    <ul>
                        <h5><strong>  Historique: </strong></h5>
                        <li> Être informé des nouveautés liées aux différents projets traités </li>
                        
                    </ul>
                </div>
            </ul>
            </div>
        </div>
    @endif

  

    
@endsection