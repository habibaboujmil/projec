
<div id="create" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Ajouter</h4>
            </div>
      		<div class="modal-body">

         		<form id="formRegister" method="POST" action="{{url('admin/accounts/clients')}}" accept-charset="UTF-8">
         		{{csrf_field()}}
           		<div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group {{ form_error_class('firstname', $errors) }}">
                            <label>Nom</label>
                            <input type="text" class="form-control" name="firstname" placeholder="Enter First Name" value="{{ old('firstname') }}">
                            {!! form_error_message('firstname', $errors) !!}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group {{ form_error_class('lastname', $errors) }}">
                            <label>Prénom</label>
                            <input type="text" class="form-control" name="lastname" placeholder="Enter Last Name" value="{{ old('lastname') }}">
                            {!! form_error_message('lastname', $errors) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group {{ form_error_class('email', $errors) }}">
                    <label>Adresse Email  <i class="fa fa-envelope"></i></label>

                    <div class="input-group col-md-12">
                        <input type="text" class="form-control" id="id-email" name="email" placeholder="Email Address" >
                    </div>
                    {!! form_error_message('email', $errors) !!}
                </div>

                <div class="form-group {{ form_error_class('password', $errors) }}">
                    <label>Mot de Passe  <i class="fa fa-lock"></i></label>
                    <div class="input-group col-md-12">
                        <input type="password" class="form-control" id="id-password" name="password" placeholder="Password" value="{{ old('password') }}" >
                    </div>
                    {!! form_error_message('password', $errors) !!}
                </div>

                <div class="form-group {{ form_error_class('password_confirmation', $errors) }}">
                    <label>Confirmer Mot de Passe <i class="fa fa-lock"></i></label>
                    <div class="input-group col-md-12">
                        <input type="password" class="form-control" id="id-password_confirmation" name="password_confirmation" placeholder="Password Confirm" value="{{ old('password_confirmation') }}" >
                    </div>
                    {!! form_error_message('password_confirmation', $errors) !!}
                </div>
                
                <div class="col-12 text-right">
                    <button type="submit" class="btn btn-primary btn-submit">Ajouter</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    @parent

<script type="text/javascript">
                	    $(document).on('submit', '#formRegister', function(e) {  
        e.preventDefault();
          
        $('input+small').text('');
        $('input').parent().removeClass('has-error');
          
        $.ajax({
            method: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: "json"
        })

        .done(function(data) {
            $('.alert-success').removeClass('hidden');
            $('#myModal').modal('hide');
        })
        .fail(function(data) {
            $.each(data.responseJSON, function (key, value) {
                var input = '#formRegister input[name=' + key + ']';
                $(input + '+small').text(value);
                $(input).parent().addClass('has-error');
            });
        });
    });

})
                </script>        
                @endsection

