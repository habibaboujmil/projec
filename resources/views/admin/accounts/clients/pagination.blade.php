<table id="tbl-list" data-server="false" class="dt-table table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Nom </th>
        <th>Email</th>
        
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $item)
        <tr>
            <td>{{ $item->fullname }}</td>
            <td>{{ $item->email }}</td>
            <!--<td>{{ $item->roles_string }}</td>-->
            <td>
                
                    <!--<div class="btn-group">
                        <a href="/admin/accounts/clients/{{$item->id}}" class="btn btn-default btn-xs" data-toggle="tooltip" title="Show {{$item->fullname}}">
                            <i class="fa fa-eye"></i>
                        </a>
                    </div>

                    <div class="btn-group">
                        <a href="/admin/accounts/clients/{{$item->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit {{$item->fullname}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </div>-->

                    <div class="btn-group">
                        <form id="form-delete-row{{ $item->id }}" method="POST" action="/admin/accounts/clients/{{ $item->id }}">
                            <input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="{{ csrf_token() }}">
                            <input name="_id" type="hidden" value="{{ $item->id }}">
                            <a data-form="form-delete-row{{ $item->id }}" class="btn btn-danger btn-xs btn-delete-row" data-toggle="tooltip" title="Delete {{ $item->fullname }}">
                                <i class="fa fa-trash"></i>
                            </a>
                        </form>
                    </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@include('admin.partials.pagination_footer')