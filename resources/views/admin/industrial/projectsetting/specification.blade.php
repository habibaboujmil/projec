@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid" >
            <div class="box-header with-border ">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i> Spécification</span>
                    <span></span>
                </h3>
            </div>

            <div class="box-body">
            	<form action="{{url('admin/projectsetting/specification/edit')}}" method="post">
                    <input type="hidden" name="_method" value="PUT">
                    {{csrf_field()}}

	                <div>
	                	<textarea  style="width: 100%;text-align: center;" name="title" class="form-control" >
                            @if(isset($specification->title))
                            {{$specification->title}}
                            @endif
	                	</textarea>
	                	<textarea style="width: 100%;height: 700px" name="content"class="form-control">
                            @if(isset($specification->content))
                            {{$specification->content}}
                            @endif
	                	
	                    </textarea>
	                </div>
	                <div  style="float: right;">
	                	<button class="btn btn-primary" type="submit" name="form-control"><i class="fa fa-pencil-square-o"
	                		></i>Editer</button>
	                </div>
	            </form>
               

            </div>
           
        </div>
    </div>
</div>

@endsection
