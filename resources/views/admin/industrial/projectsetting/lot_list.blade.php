@extends('layouts.admin')

@section('content')

       

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i></span>
                    <span>Liste des lots</span>
                </h3>
            </div>
            @if(session()->has('errors'))
                    <div style="color: red; text-align:center;">
                      <strong> Minimum un lot doit etre coché !</strong>
                    </div>
            @endif
            <div class="box-body">
                <form action="{{url('admin/projectsetting/lot/edit') }}" method="post" name="url"  >
                    <input type="hidden" name="_method" value="PUT">
                    {{csrf_field()}}
                    <table id="tbl-list" data-server="false" class="dt-table table  table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="15px" >N°</th>
                            <th width="15px" style="text-align: center;"><i class="fa fa-pencil"data-toggle="tooltip" title="Editer"></i></th>
                            <th> Nom</th>
                            <th></th>
                            
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <b>NB:</b> Ce champ sera affiché dans: <br> -liste des choix des lots <br> -pied de page de bordereaux et CPTP
                                </td>
                                <td>
                                    <b>NB:</b> Ce champ sera affiché dans:  <br> -l'en-tête du bordereaux et CPTP
                                </td>


                            </tr>
                            @foreach($lot_list as $lot)
                             <tr>
                                <td>{{$lot->id }}</td>
                                <td>
                                    <input type="checkbox" name="lot_id[]" value="{{$lot->id }}">
                                    
                                </td>
                                <td>
                                    <input type="text" style="width: 100%; border: none;" name="lot_name[{{$lot->id}}][]" class="form-control" value="{{$lot->lot_name}}">
                                     
                                </td>
                                <td>
                                     <input type="text" style="width: 100%; border: none;" name="tot_name[{{$lot->id}}][]"class="form-control" value="{{$lot->tot_name}}">
                                 </td>
                             </tr>
                             @endforeach
                        </tbody>
                    </table>
                    <div  style="float: right;">
                        <button class="btn btn-warning" type="submit" name="form-control"><i class="fa fa-pencil-square-o"
                            ></i>Editer</button>
                    </div>

                </form>
            </div>

                
            </div>

        </div>
    </div>


    
    @endsection
    

