@extends('layouts.admin')

@section('content')


<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i></span>
                    <span>Liste des Clauses Communes / {{$lot->lot_name}}</span>
                </h3>
            </div>
            <div class="well well-sm well-toolbar">
                <a href="{{url('admin/industrial/clausescommune/'.$type.'/'.$lot->id.'/create')}}"class="btn btn-primary" type="button" >
                    <span class="btn-label"><i class="fa fa-plus"></i></span>Ajouter une Clause 
                </a>
                
            </div>
            <div class="box-body">
                @foreach($clause as $clause)
                <form action="{{url('admin/industrial/communclause/edit')}}" method="post">
                    <input type="hidden" name="_method" value="PUT"  class="masked">
                    {{csrf_field()}}
	                <div class="panel panel-default">
					    <div class="panel-heading" style="background:#DCDCDC">
					    	<i class="fa fa-chevron-right" data-toggle="collapse" data-target="#{{$clause->id}}" onclick="changeicon('icon{{$clause->id}}')" id="icon{{$clause->id}}"></i>
					    	<b> 
					    		<textarea class="header_txt" name="title[]" onmouseup="auto_grow(this)" > {{$clause->title}}</textarea>
					    	</b>
					    	<button class="edit" type="submit">
					    		<i class="fa fa-pencil-square-o" aria-hidden="true"data-toggle="tooltip" title="Editer"></i>
					    	</button>
					    </div>
					    <div class="collapse" id="{{$clause->id}}">
					    	<div class=" panel-body">
					    	<input type="hidden" name="id[]" value="{{$clause->id}}" >
						    <textarea class="body_txt" name="clause[]" onmouseup="auto_grow(this)" >{{$clause->clause}} </textarea>
						    @if(!empty($clause->clause_file))
						    <img  src= "{{ asset('storage/'.$clause->clause_file) }}">
						    @endif
						    <p class="masked">{{$child1=App\models\commun_clause::child_clause($clause->id)}}</p>
					    	
						    	@foreach($child1 as $child)
						    	<table width="100%" >
	                            	<thead>
							    		<tr>
								    		<th width="20px">
								    			<i class="fa fa-chevron-right" data-toggle="collapse" data-target="#{{$child->id}}" onclick="changeicon('icon{{$child->id}}')" id="icon{{$child->id}}"></i> 
								    		</th>
								    		<th >
								    			<input type="hidden" name="id[]" value="{{$child->id}}" >
									    		<textarea class="body_txt" name="title[]" onmouseup="auto_grow(this)"> {{$child->title}} 
									    		</textarea>
									    		@if(!empty($clause->clause_file))
											    <img  src= "{{ asset('storage/'.$child->clause_file) }}">
											    @endif
								    		 </th>
							    	    </tr>
						    	    </thead>
							        <tbody class="collapse" id="{{$child->id}}">
							    	<th width="20px"></th>
							    	<td>
							    		<textarea class="body_txt" name="clause[]" onmouseup="auto_grow(this)">{{$child->clause}}
							    		</textarea>
							    	</td>
							    	</tbody>	
							    </table>
	                            <p class="masked">{{$child2=App\models\commun_clause::child_clause($child->id)}} {{$a='a'}}
	                            </p>
	                            @if(!empty($child2))
	                            @foreach($child2 as $child2)
	                            <table width="100%" >
	                            	<thead>
							    		<tr>
							    			<th width="20px"></th>
								    		<th width="20px">
								    			<i class="fa fa-chevron-right" data-toggle="collapse" data-target="#{{$child2->id}}" onclick="changeicon('icon{{$child2->id}}')" id="icon{{$child2->id}}"></i> 
								    		</th>
								    		<th >
								    			<input type="hidden" name="id[]" value="{{$child2->id}}" >
									    		<textarea class="body_txt" name="title[]" onmouseup="auto_grow(this)"> {{$child2->title}} 
									    		</textarea>
									    		@if(!empty($clause->clause_file))
											    <img  src= "{{ asset('storage/'.$child2->clause_file) }}">
											    @endif

								    		 </th>
							    	    </tr>
						    	    </thead>
							        <tbody class="collapse" id="{{$child2->id}}">
							    	<th width="20px"></th>
							    	<th width="20px"></th>
							    	<td>
							    		<textarea class="body_txt" name="clause[]" onmouseup="auto_grow(this)">{{$child2->clause}}
							    		</textarea>
							    	</td>
							    	</tbody>	
							    </table>
	                            @endforeach
	                            @endif
						    	@endforeach
					        </div>
					    </div>
					</div>
				</form>
                @endforeach
            </div>
        </div>
    </div>
</div>

<style type="text/css">
	.header_txt{border: none;width:90%; height: 1.3em; overflow:hidden;background:#DCDCDC;resize: none;}
	.body_txt{border: none;width:90%; height: 1.3em; overflow:hidden;resize: none;}
	.edit{float: right; border: none; color: black;background:none }
	.masked{display: none;}
	img{width: 400px; height: 400px;  display: block;margin-left: auto;  margin-right: auto}

</style>

<script type="text/javascript">

 $(document).ready(function()
 	{ $('[data-toggle="tooltip"]').tooltip();}); 

function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}


function changeicon (iconID)
{
	if(document.getElementById(iconID).className=="fa fa-chevron-right"){
    document.getElementById(iconID).className = "fa fa-chevron-down";
    }
    else{document.getElementById(iconID).className = "fa fa-chevron-right";}
}
</script>
    @endsection
    

