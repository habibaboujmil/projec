@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
          <div class="well well-sm well-toolbar menu">
                <i class="fa fa-plus" style="margin-left: 20px"  onclick="duplicate('0')"></i>
                <strong >Niveau 1.1</strong>
                <i class="fa fa-plus" style="margin-left: 20px"  onclick="duplicatel3('child0')"></i>
                <strong >Niveau 1.1.a</strong>
            </div>
          <div class="box-body">
           <form action="{{url('admin/industrial/communclause')}}"
                 method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="building_id" value="{{$building_id}}">
            <input type="hidden" name="lot_id" value="{{$lot_id}}">
              <label for="l1_title">1. Titre </label>
              <input type="text" name="l1_title" class="form-control" value="{{old('l1_title')}}" >
              <div id="clone_l1">
                <label for="l1_clause">Clause</label>
                <textarea name="l1_clause" class="form-control" value="{{old('l1_clause')}}" onmouseup="auto_grow(this)" id="l1_clause">
                </textarea>
                <input type="file" name="l1_file" class="file">
              </div>
            <div class="form-group " >
              <div class="divclone_l2 "  id="0" >
                 <input type="hidden" id="test0" name="test" value="0">
                  <label for="l2_title">1.1:  Titre </label>
                  <input id="0" type="text" name="l2_title[]" class="form-control" >
                  <div id="clone_l2">                 
                   <label for="l2_clause">Clause</label>
                   <textarea name="l2_clause[]" class="form-control" onmouseup="auto_grow(this)" id="l2_clause" >
                   </textarea> 
                  </div>

                  <input type="file" name="l2_file[]" class="file">
                  
              </div> 
              <div class="divclone_l3"  id="child0">
                    <label for="l3_title[]">1.1.a:  Titre </label>
                    <input id="l3_title"  type="text" name="l3_title[0][]" class="form-control" value="{{old('l3_title')}}">
                    <label for="l3_clause[]">Clause</label>
                    <textarea id="l3_clause" name="l3_clause[0][]" class="form-control" value="{{old('l1_clause
                    ')}}" onmouseup="auto_grow(this)">
                    </textarea>
                    <input id="l3_file" type="file" name="l3_file[0][]" class="file">
                    
                </div> 
            </div>
            <div style="margin: 10px">
              <p> <b>Cette clause sera placée dans: </b></p>
              <div style="margin: 10px 25px">
                <input type="radio" name="position" value="0">La première partie de document<br>
                <input type="radio" name="position" value="1">La fin du document<br>
              </div>
            </div>
            <div>
              <input class="btn btn-primary" type="submit" name="Clause" value="sauvgarder">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
  var count=1;
function duplicate(i)
  {
    document.getElementById('test0').value = count++;
    var original = document.getElementById(i);
    var clone = original.cloneNode(true);
    original.parentNode.appendChild(clone);
    clone.find("input").val("");
    clone.find("textarea").val("");
  }




var add=1;
  function duplicatel3(i)
  {
    var original = document.getElementById(i);
    var clone = original.cloneNode(true);
    clone.getElementsByTagName('input')[0].setAttribute('id', 'l3_title' +add);
    clone.getElementsByTagName('input')[1].setAttribute('id', 'l3_file' +add);
    clone.getElementsByTagName('textarea')[0].setAttribute('id', 'l3_clause' +add);
    original.parentNode.appendChild(clone);
     index = document.getElementById('test0').value ;
     document.getElementById('l3_title'+ add).setAttribute('name', 'l3_title['+index+'][]');
     document.getElementById('l3_clause'+ add).setAttribute('name', 'l3_clause['+index+'][]');
     document.getElementById('l3_file'+ add).setAttribute('name', 'l3_file['+index+'][]');
    
     add++;



  }

function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}

$('.menu').addClass('original').clone().insertAfter('.menu').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();

scrollIntervalID = setInterval(stickIt, 10);

function stickIt() {

  var orgElementPos = $('.original').offset();
  orgElementTop = orgElementPos.top;               

  if ($(window).scrollTop() >= (orgElementTop)) {
    // scrolled past the original position; now only show the cloned, sticky element.

    // Cloned element should always have same left position and width as original element.     
    orgElement = $('.original');
    coordsOrgElement = orgElement.offset();
    leftOrgElement = coordsOrgElement.left;  
    widthOrgElement = orgElement.css('width');
    $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
    $('.original').css('visibility','hidden');
  } else {
    // not scrolled past the menu; only show the original menu.
    $('.cloned').hide();
    $('.original').css('visibility','visible');
  }
  }
</script>
<style type="text/css">
  .divclone_l2{margin-left: 30px; margin-top: 20px; display: block;}
  .divclone_l3{margin-left: 60px;margin-top: 20px; display: block;}
  .file{float: right;cursor: pointer;}
</style>
          @endsection
     
  