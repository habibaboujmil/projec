@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
          <div class="well well-sm well-toolbar menu">
            <i class="fa fa-plus" style="margin-left: 20px"  onclick="duplicate('0')"></i>
          </div>
          <div class="box-body">
            <form action="{{url('admin/industrial/project/cfo/input/save')}}"
                 method="post" enctype="multipart/form-data">
            {{csrf_field()}} 
            
            <div class="form-group ">
              <strong>{{$clause[0]->title}}<br></strong>
               {{$clause[0]->clause}} <input type="text" name="input[]" value="{{$project_name}}">
            </div>
            <div class="form-group ">
              <strong>{{$clause[1]->title}}<br></strong>
               {{$clause[1]->clause}} <input type="text" name="input[]">
            </div>
            <div class="form-group ">
              <strong>{{$clause[2]->title}} <br></strong>
              {{$clause[2]->clause}}
              <select name="input[]">
                <option value="TT">TT</option>
                <option value="TNS">TNS</option>
                <option value="TNC">TNC</option>
                <option value="IT">IT</option>
              </select> 
            </div>
            <div class="form-group ">
              <strong>{{$clause[3]->title}} <br></strong>
              {{$clause[3]->clause}}<input type="text" name="input[]">
            </div>
            <div class="form-group ">
              <strong>{{$clause[2]->title}} <br></strong>
              {{$clause[4]->clause}}<input type="text" name="input[]">
            </div>
            <div>
              <input class="btn btn-primary" type="submit" value="sauvgarder">
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>


@endsection