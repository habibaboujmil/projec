<style type="text/css">
	.page_break { page-break-before: always; }
	.center{text-align: center}
	.cadre{}
	 #header_cptp { position: fixed; left: 0px; top: -39px; right: 0px; font:Calibri Light;font-size: 11px, }
	 #footer { position: fixed; left: 0px; top: 940px; right: 0px; height: -35px; font:Verdana ; font-size: 12px }
</style>

<div id="header_cptp">
        <p style="float:left;">CPTP {{$lot_info->tot_name}}</p>
        <p style="float:right;margin-top: 10px">{{$project->name}}</p>
</div>

<div id="footer" style="margin-left: 0.5cm; ">
	<p>TELEC Engeneering
	<br>{{$project->reference}}_CPTP_{{$lot_info->lot_name}}</p>
</div>

<script type="text/php">
    if (isset($pdf)) {
        $text = "page {PAGE_NUM} / {PAGE_COUNT}";
        $size = 10;
        $font = $fontMetrics->getFont("Verdana");
        $width = $fontMetrics->get_text_width($text, $font, $size);
        $x = $pdf->get_width() -70; 
        $y = $pdf->get_height() - 35;
        $pdf->page_text($x, $y, $text, $font, $size);
    }
</script>
