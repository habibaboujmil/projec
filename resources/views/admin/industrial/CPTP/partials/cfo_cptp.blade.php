<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CPTP</title>
    <style type="text/css">
	i{display: none;}
	.l1{}
	.l2{margin-left:18px}
	.l3{margin-left: 30px}
	.inter_ligne{margin-top: 10px}
	img{width: 700px;}
	body{font-size: 12px; font-family: arial,sans-serif}
</style>
</head>
<body>
	@include('admin.industrial.CPTP.partials.header_CPTP') 
	<!--sommaire-->
	<div style="text-align: center; margin: 30px"> <b>S O M M A I R E</b></div>
	<div style="margin: 0.5cm">
		<i> {{$compt1=1}}</i>
		
		@foreach($first_clauses as $first)
		<i> {{$compt2=1}}{{$child1=App\models\commun_clause::child_clause($first->id)}}</i>
		<!-- clause level 1-->
		<div class="inter_ligne">
			<strong>{{$compt1++}}. {{$first->title}}</strong>

			@foreach($child1 as $child1)
			<div class="l2 inter_ligne">
				<strong > {{$compt1 -1}}.{{$compt2++}}.   {{$child1->title}} </strong>
	            <!-- clause level 2-->
				<i>{{$compt3='a'}}{{$child2=App\models\commun_clause::child_clause($child1->id)}}</i>
				<div class="inter_ligne">
					@foreach($child2 as $child2)
					<div class="l3">{{$compt1 -1}}.{{$compt2 -1}}.{{$compt3++}}. {{$child2->title}}
					</div>
					@endforeach
				</div>
			</div>
			@endforeach
		</div>
		@endforeach

		


		@foreach($last_clauses as $laste)
		<i> {{$compt2=1}}{{$child1=App\models\commun_clause::child_clause($laste->id)}}</i>
		<!-- clause level 1-->
		<div class="inter_ligne">
			<strong>{{$compt1++}}. {{$laste->title}}</strong>

			@foreach($child1 as $child1)
			<div  class="l2 inter_ligne">
				<strong > {{$compt1 -1}}.{{$compt2++}}.   {{$child1->title}} </strong>
	            <!-- clause level 2-->
				<i>{{$compt3='a'}}{{$child2=App\models\commun_clause::child_clause($child1->id)}}</i>
				<div class="inter_ligne">
					@foreach($child2 as $child2)
					<div class="l3">{{$compt1 -1}}.{{$compt2 -1}}.{{$compt3++}}. {{$child2->title}}
					</div>
					@endforeach
				</div>
			</div>
			@endforeach
		</div>
		@endforeach
	</div>




	<!-- CPTP content -->
<div style="margin: 0.5cm"  class="page_break">
	<i> {{$compt1=1}} {{$input=0}}</i>
	<!--first commun clause -->
	
	@foreach($first_clauses as $first)
		<i> {{$compt2=1}}{{$child1=App\models\commun_clause::child_clause($first->id)}}</i>
		<!-- clause level 1-->
		<div>
		<strong>{{$compt1++}}. {{$first->title}}</strong>
		<p>
			{!!nl2br(e($first->clause))!!}
		    @if($first->with_var == 1)
			 {!!nl2br(e($cptp_input[$input]))!!}
			 <i>@if($input<4){{($input++)}}@endif</i>
			@endif
		</p>
		@if(!empty($first->clause_file))
			<div >
				<img  src="{{public_path('storage/'.$first->clause_file)}}">
			</div>
		@endif

		@foreach($child1 as $child1)
			<div>
				<p class="l3"><strong>{{$compt1 -1}}.{{$compt2++}}.   {{$child1->title}} </strong></p>
				<p>{!!nl2br(e($child1->clause))!!}
					@if($child1->with_var == 1)
			 			{!!nl2br(e($cptp_input[$input]))!!}
			 			<i>@if($input<4){{($input++)}}@endif</i>
					@endif
				</p>
        		<!-- clause level 2-->
				<i>{{$compt3='a'}}{{$child2=App\models\commun_clause::child_clause($child1->id)}}</i>
				@foreach($child2 as $child2)
					<div>
						<b class="l3">{{$compt1-1}}.{{$compt2-1}}.{{$compt3++}}. {{$child2->title}} </b>
						<p>{!!nl2br(e($child2->clause))!!}</p>
					</div>
				@endforeach
			</div>
		@endforeach
	</div>
@endforeach






        <!-- project clause -->
        
        

<i>{{$index=0}}{{$index1=0}}</i>
 @foreach ($articles as $arti => $article)
    @if(isset($article_content[$arti]))
        @foreach($article_content[$arti]->l1_title as $art_l1 => $l1_title)

	        @if(isset($l1_title)) <strong>{{$compt1++}}. {{$l1_title}}</strong> @endif
	        <p>{!!nl2br(e($article_content[$arti]->l1_clause[$art_l1]))!!}</p>
            <i>{{$compt2=1}}</i>
            @if(isset($article_content[$arti]->l1_file[$art_l1]))
				<div >
				<img  src="{{public_path('storage/'.$article_content[$arti]->l1_file[$art_l1])}}" >
				</div>
			@endif
            
            @if(isset($article_content[$arti]->l2_title[$art_l1]))
	            @foreach($article_content[$arti]->l2_title[$art_l1] as $art_l2 => $l2_title)
		            @if(isset($l2_title))<strong class="l2">{{$compt1 -1}}.{{$compt2++}}. {{$l2_title}}</strong> @endif
		            <p>{!!nl2br(e($article_content[$arti]->l2_clause[$art_l1][$art_l2]))!!}</p>
		                @if(isset($article_content[$arti]->l2_file[$art_l1][$art_l2]))
						<div >
						<img src="{{public_path('storage/'.$article_content[$arti]->l2_file[$art_l1][$art_l2])}}">
						</div>
						@endif
					
	                @if(isset($article_content[$arti]->l3_title[$art_l1][$art_l2]))
			            @foreach($article_content[$arti]->l3_title[$art_l1][$art_l2] as $art_l3 => $l3_title)
				            @if(isset($l3_title))<strong class="l3">{{$compt1-1}}.{{$compt2-1}}.{{$compt3++}}. {{$l3_title}}</strong> @endif
				            <p>{!!nl2br(e($article_content[$arti]->l3_clause[$art_l1][$art_l2][$art_l3]))!!}</p>
			        	@endforeach
		        	@endif
		        @endforeach
	        @endif
        @endforeach
    @endif

<!--traitement des equip -->
	@if(!empty($equipments[$index]))
	 	@while(($index<$break)and($equipments[$index]->article_id==$article->id) )
	     	@if(isset($equipment_content[$index]))
	    		@foreach($equipment_content[$index]->l1_title as $l1 => $l1_title)
	    			@if(isset($l1_title)) <strong>{{$compt1++}}. {{$l1_title}}</strong> @endif
	     			<p>{!!nl2br(e($equipment_content[$index]->l1_clause[$l1]))!!}</p>
	     			@if(isset($equipment_content[$index]->l1_file[$l1]))
						<div >
						<img src="{{public_path('storage/'.$equipment_content[$index]->l1_file[$l1])}}">
						</div>
					@endif
	         
	       			@foreach($equipment_content[$index]->l2_title[$l1] as $l2 => $l2_title)
	        			@if(isset($l2_title))<strong class="l2">{{$compt1 -1}}.{{$compt2++}}. {{$l2_title}}</strong> @endif
	       				<p>{!!nl2br(e($equipment_content[$index]->l2_clause[$l1][$l2]))!!}</p>
	       				@if(isset($equipment_content[$index]->l2_file[$l1][$l2]))
							<div >
								<img src="{{public_path('storage/'.$equipment_content[$index]->l2_file[$l1][$l2])}}">
							</div>
					    @endif
	             		<i> {{$compt3='a'}} </i>

            			@foreach($equipment_content[$index]->l3_title[$l1][$l2] as $l3 => $l3_title)
           					@if(isset($l3_title))<strong class="l3">{{$compt1-1}}.{{$compt2-1}}.{{$compt3++}}. {{$l3_title}}</strong> @endif
            				<p>{!!nl2br(e($equipment_content[$index]->l3_clause[$l1][$l2][$l3]))!!}</p>
    					@endforeach
	    			@endforeach
	    		@endforeach
	    	@endif

		    @if(($sousequipments[$index1]!=NULL))
		    	@while($sousequipments[$index1]->equi_id == $equipments[$index]->id) 
		    	 
		    		@if(isset($sousequip_content[$index1]))
		     			@foreach($sousequip_content[$index1]->l1_title as $l1 => $l1_title)
		    				@if(isset($l1_title)) <strong>{{$compt1++}}. {{$l1_title}}</strong> @endif
		     				<p>{!!nl2br(e($sousequip_content[$index1]->l1_clause[$l1]))!!}</p>
		         
		        			@foreach($sousequip_content[$index1]->l2_title[$l1] as $l2 => $l2_title)
		        				@if(isset($l2_title))<strong class="l2">{{$compt1 -1}}.{{$compt2++}}. {{$l2_title}}</strong> @endif
		        				<p>{!!nl2br(e($sousequip_content[$index1]->l2_clause[$l1][$l2]))!!}</p>
		            			<i> {{$compt3='a'}} </i>

		            			@foreach($sousequip_content[$index1]->l3_title[$l1][$l2] as $l3 => $l3_title)
		            				@if(isset($l3_title))<strong class="l3">{{$compt1-1}}.{{$compt2-1}}.{{$compt3++}}. {{$l3_title}}</strong> @endif
		            				<p>{!!nl2br(e($sousequip_content[$index1]->l3_clause[$l1][$l2][$l3]))!!}</p>
		    					@endforeach
		    				@endforeach
		    			@endforeach
		    		@endif

		    		<i>{{$index1++}}</i>
		    		@if ($index1==$sous_break -1) @break  @endif
		    	@endwhile
		    @endif

			<i>{{$index++}}</i>
	 		@if ($index==$break) @break  @endif
	 
 		@endwhile
 	@endif
 @endforeach






<!--last commun clauses -->
@foreach($last_clauses as $last)
	<i> {{$compt2=1}}{{$child1=App\models\commun_clause::child_clause($last->id)}}</i>
<!-- clause level 1-->
	<div>
		<strong>{{$compt1++}}. {{$last->title}}</strong>
		<p>{!!nl2br(e($last->clause))!!}</p>
		@if(!empty($last->clause_file))
			<div >
				<img style="width: 680px" src="{{public_path('storage/'.$last->clause_file)}}">
			</div>
		@endif
		@foreach($child1 as $child1)
			<div>
				<p style="margin-left: 30px"><strong>{{$compt1 -1}}.{{$compt2++}}.   {{$child1->title}} </strong></p>
				<p>{!!nl2br(e($child1->clause))!!}</p>
            <!-- clause level 2-->
				<i>{{$compt3='a'}}{{$child2=App\models\commun_clause::child_clause($child1->id)}}</i>
				@foreach($child2 as $child2)
					<div>
						<b style="margin-left: 60px">{{$compt1-1}}.{{$compt2-1}}.{{$compt3++}}. {{$child2->title}} </b>
						<p>{!!nl2br(e($child2->clause))!!}</p>
					</div>
				@endforeach
			</div>
		@endforeach
	</div>
@endforeach
</div>
	
<div style="display:inline-block; margin: 30px;">
	<div style=" float: left;margin: 10px;">
	DRESSER PAR<br>
	Telec Engineering<br>
	Tunis, le .......................
    </div>
    <div style=" float: right;margin: 10px;">
	LU ET ACCEPTER PAR<br>
	L'Entrepreneur<br>
	Tunis, le .......................
    </div>
</div>
<div style="margin: 90px; text-align: center;" >
	<p>VU ET APPROUVER PAR<br>
	LE MAITRE DE L'OUVRAGE<br>
	Tunis, le .......................</p>
</div>		
	
</body>
</html>

   #footer .page:after { content: counter(page, upper-roman); }
   </style>
  <body>
   
   <div id="footer">
     <p class="page">Page <?php $PAGE_NUM ?></p>
   </div>