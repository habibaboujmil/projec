 <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" align="center"><b> Editer </b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/industrial/equipments/edit')}}" method="post">
                    <input type="hidden" name="_method" value="PUT">
                {{csrf_field()}}
                <input type="hidden" name="equipment_id" id="id" class="form-control">
                 <div class="form-group" style="margin-left: 16px">
                  <label for="app_display">Titre(affiché)</label>
                  <input type="text" name="app_display" class="form-control" id="app_display">
                </div>
                <div class="form-group " style="margin-left: 16px">
                    <label for="equipment_title">Titre </label>
                    <input type="text" name="equipment_title" class="form-control" id="equipment_title">
                </div>
                <div class="form-group" style="margin-left: 16px">
                    <label for="equipment_description">Designation de l'ouvrage</label>
                    <textarea name="equipment_description" class="form-control" id="equipment_description"  rows="6" cols="33" >
                    </textarea>
                </div>
                <div class="form-group">
                    <div class="col col-6">
                        <label for="equipment_unit">Unité Complète </label>
                        <input type="text" name="equipment_unit" class="form-control" id="equipment_unit"  >
                    </div>
                    <div class="col col-6">
                          <label for="equipment_unit">Unité en abréviation</label>
                          <input type="text" name="equipment_unit_abre" class="form-control" id="unit_abre"  >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col col-6">
                          <label for="equipment_price">Prix de la Fourniture </label>
                          <input type="text" name="four_price" class="form-control" id="four_price" >
                    </div>
                    <div class="col col-6">
                          <label for="equipment_price">Prix de la Pose </label>
                          <input type="text" name="pose_price" class="form-control" id="pose_price" >
                    </div>
                </div>
                <div>
                    <div class="col col-6" style="margin-top: 15px ">
                      <input type="checkbox" name="duplicate" value="1" id="duplicate" >Autoriser la duplication
                    </div>
                    <div class="col col-6" style="margin-top: 15px ">        
                      <input type="checkbox" name="descrip" value="1" id="descrip" > Autoriser l'ajout d'un texte
                    </div>
              </div>
              
            <div class="modal-footer">
                <input style="float: right; margin-top: 30px;" class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="submit" name="form-control" value="sauvgarder">
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
@section('scripts')
    
        <script type="text/javascript">
         $('#edit').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var equipment_title = button.data('title') 
      var equipment_description = button.data('description')
      var app_display= button.data('app')
      var equipment_unit= button.data('unit')
      var unit_abre= button.data('abre')  
      var four_price = button.data('four') 
      var pose_price = button.data('pose')
      var duplicate = button.data('dup')
      var descrip = button.data('desc')
      var pm = button.data('pm')
      var id = button.data('id') 

      var modal = $(this)
      modal.find('.modal-body #equipment_title').val(equipment_title);
      modal.find('.modal-body #equipment_description').val(equipment_description);
      modal.find('.modal-body #app_display').val(app_display);
      modal.find('.modal-body #equipment_unit').val(equipment_unit);
      modal.find('.modal-body #unit_abre').val(unit_abre);
      modal.find('.modal-body #four_price').val(four_price);
      modal.find('.modal-body #pose_price').val(pose_price);
      modal.find('.modal-body #id').val(id);
    })
    </script>

@endsection
