@extends('layouts.admin')

@section('content')
<style type="text/css"> .center{text-align: center;}</style>

        @if(session()->has('success'))
        <div class="alert alert-success">
          <p>{{session()->get('success')}}</p>
        </div>
        @endif

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i></span>
                   
                    <span>{{ $article->article_title }}</span>
                   
                </h3>
            </div>

            <div class="box-body">
                <div class="well well-sm well-toolbar">
                    <a href="{{url('admin/industrial/creat/'.$article->id.'- /equipments')}}" class="btn btn-primary" type="button">
                        <span class="btn-label"><i class="fa fa-fw fa-user-plus"></i></span>Ajouter un sous article
                     </a>
                     
                     <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#category" style="float: right;">
                        <span class="btn-label"><i class="fa fa-fw fa-user-plus"></i></span>Ajouter une catégorie
                     </button>
                     @include('admin.industrial.borderaux.equipments.category.add_category')
                </div>
                @if(!empty($category[0]))
                <div>
                    <h4 class="box-title">
                        <span><i class="fa fa-table"></i></span>
                        <span>Liste des categorie</span>
                    </h4>
                    <table id="tbl-list1" data-server="false" class="dt-table table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="center">N°</th>
                                <th class="center">Nom</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($category as $category)
                            <tr>
                                <td width="30px" >{{ $loop->index+1 }}</td>
                                <td>{{$category->category_title}}</td>
                                <td width="140px" class="center">
                                    <a href="{{url('admin/industrial/category'.$category->id.'-'.$article->id.'/equipments')}}" class="btn btn-primary" > <i class="fa fa-table" data-toggle="tooltip" title="Détail"></i></a>
                                    <button  class="btn btn-warning " type="button" data-toggle="modal" data-target="#edit_categ" data-id="{{$category->id}}" data-title="{{$category->category_title}}">
                                    <i class="fa fa-pencil-square-o"data-toggle="tooltip" title="Info / Editer"></i></button>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_categ" data-id="{{$category->id}}" ><i class="fa fa-trash-o"data-toggle="tooltip" title="Supprimer"></i></button>
                                 </td>
                            </tr>
                         @endforeach
                        </tbody>
                    </table>
                    @include('admin.industrial.borderaux.equipments.category.destroy_categ')
                    @include('admin.industrial.borderaux.equipments.category.edit_categ')
                </div>
                @endif
                <div>
                    <h4 class="box-title">
                        <span><i class="fa fa-table"></i></span>
                        <span>Liste des sous articles</span>
                    </h4>
                     <table id="tbl-list2" data-server="false" class="dt-table table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="center" width="30px">N°</th>
                            <th class="center">Nom</th>
                            <th class="center">Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($equipment as $equipment)
                             <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{$equipment->app_display}}
                                <td width="125px">
                                    @if($article->level == 1 and empty($equipment->equipment_unit))
                                    <a href="{{url('admin/industrial/sousequipments/'.$equipment->id)}}" class="btn btn-primary" > <i class="fa fa-table" data-toggle="tooltip" title="Liste des equipment"></i></a>
                                    @else <i style="margin-left: 43px"></i>
                                    @endif
                                    <button  class="btn btn-warning " type="button" data-toggle="modal" data-target="#edit" data-id="{{$equipment->id}}" data-title="{{$equipment->equipment_title}}" data-description="{{$equipment->equipment_description}}" data-unit="{{$equipment->equipment_unit}}" data-abre="{{$equipment->equipment_unit_abre}}"
                                    data-four="{{$equipment->four_price}}" data-pose="{{$equipment->pose_price}}" data-dup="{{$equipment->duplicate}}"
                                    data-desc="{{$equipment->descrip}}"
                                    data-app="{{$equipment->app_display}}">
                                    <i class="fa fa-pencil-square-o"data-toggle="tooltip" title="Info / Editer"></i></button>
                                    
                                    <button style="float: right;" type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete" data-id="{{$equipment->id}}" ><i class="fa fa-trash-o"data-toggle="tooltip" title="Supprimer"></i></button> 
                                    @include('admin.industrial.borderaux.equipments.destroy_equipment')
                                 </td>
                            </tr>
                            @endforeach  
                        </tbody>
                    </table>
                    @include('admin.industrial.borderaux.equipments.edite_equipment')
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.industrial.borderaux.equipments.category.edit_categ')
<script type="text/javascript">
    $(document).ready(function()
    {
    $('[data-toggle="tooltip"]').tooltip();   
    }); 
</script>
@endsection
    

