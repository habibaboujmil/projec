<div class="modal fade" id="category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter un article</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="{{url('admin/industrial/categorie')}}" method="post">
        {{csrf_field()}}
          <div class="form-group ">
          @if (!isset($equip_id)) 
          <input type="hidden" name="article_id" class="form-control" value="{{ $article->id }}"  >
          @else
          <input type="hidden" name="equip_id" class="form-control" value="{{ $equip_id }}" >
          @endif
        </div>
        <div class="form-group ">
          <label for="article_title">Nom de la category </label>
          <input type="text" name="category_title" class="form-control" required>
        </div>
      <div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
        <input class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="submit" name="form-control" value="sauvgarder">
        </div>
      </form>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>
</div>