<div class="modal fade" id="delete_categ" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><strong>Attention</strong> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <strong> Voulez-vous vraiment supprimer cette category! </strong> 
      </div>
      <div class="modal-footer">
        
      <form action="{{url('admin/industrial/categorie'.$category->id)}}" method="post">
        {{csrf_field()}}
        {{method_field('DELETE')}}
        <input type="text" name="category_id" id="id">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
        <button type="submit" class="btn btn-warning">supprimer</button>
      </form>
        {{$category->id}}
      </div>
    </div>
  </div>
</div>

 @section('scripts')
    <script type="text/javascript">
         $('#delete_categ').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var category_id = button.data('id')
      var modal = $(this)
      modal.find('.modal-body #id').val(category_id);
     
    })
    </script>
@endsection