
 <div class="modal fade" id="edit_categ" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" align="center"><b> Editer </b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/industrial/categorie/edit')}}" method="post">
                  <input type="hidden" name="_method" value="PUT">
                    {{csrf_field()}}
                    <input type="hidden" name="categ_id" id="id" class="form-control">
                    <div class="form-group ">
                      <label for="category_title">Nom de la category </label>
                      <input type="text" name="category_title" class="form-control" id="category_title" required>
                    </div>
                    <div class="modal-footer">
                      <input style="float: right; margin-top: 30px;" class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="submit" name="form-control" value="sauvgarder">
                  </div>
                </form>
              </div>
          </div>
        </div>
</div>
@section('scripts')
    <script type="text/javascript">
         $('#edit_categ').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var category_title = button.data('title')
      var id = button.data('id')
      var modal = $(this)
      modal.find('.modal-body #category_title').val(category_title);
      modal.find('.modal-body #id').val(id);
     
    })
    </script>
@endsection
