@extends('layouts.admin')
@section('content')
<style type="text/css"> .center{text-align: center;}</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i></span>
                   
                    <span>{{ $category->category_title }}</span>
                   
                </h3>
            </div>

            <div class="box-body">
                <div class="well well-sm well-toolbar">
                    <a href="{{url('admin/industrial/creat/'.$article->id.'-'.$category->id.'/equipments')}}" class="btn btn-primary" type="button">
                        <span class="btn-label"><i class="fa fa-fw fa-user-plus"></i></span>Ajouter un sous article
                     </a>
                     
                     
                </div>
                <div>
                    <h4 class="box-title">
                        <span><i class="fa fa-table"></i></span>
                        <span>Liste des sous articles</span>
                    </h4>
                    <table id="tbl-list" data-server="false" data-page-length="25" class="dt-table table nowrap table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="center">N°</th>
                            <th class="center">Nom</th>
                            <th class="center">Description</th>
                            <th class="center">Unité</th>
                            <th class="center">Prix<br>fourniture </th>
                            <th class="center">Prix<br>pose </th>
                            <th class="center">Dup-<br>lication</th>
                            <th class="center">Ajout<br>texte </th>
                            <th class="center">Action </th>
                        </tr>
                        </thead>
                        <tbody> 
                         @foreach($equipment as $equipment)
                             <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{$equipment->equipment_title}}</td>
                                <td>{{$equipment->equipment_description}}</td>
                                <td>{{$equipment->equipment_unit}}</td>
                                <td>{{$equipment->four_price}}</td>
                                <td>{{$equipment->pose_price}}</td> 
                                <td></td>
                                <td></td>
                                <td width="140px" class="center">
                                    <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#edit" data-id="{{$equipment->id}}" data-title="{{$equipment->equipment_title}}" data-description="{{$equipment->equipment_description}}" data-unit="{{$equipment->equipment_unit}}" data-abre="{{$equipment->equipment_unit_abre}}"
                                    data-four="{{$equipment->four_price}}" data-pose="{{$equipment->pose_price}}" data-dup="{{$equipment->duplicate}}"
                                    data-desc="{{$equipment->descrip}}" data-calibre="{{$equipment->calibre}}"  >
                                    <i class="fa fa-pencil-square-o"data-toggle="tooltip" title="Editer"></i></button>
                                    
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete" data-id="{{$equipment->id}}" ><i class="fa fa-trash-o"data-toggle="tooltip" title="Supprimer"></i></button> 
                                    @include('admin.industrial.borderaux.equipments.destroy_equipment')
                                 </td>
                            </tr>
                            @endforeach  
                        </tbody>
                    </table>
                  @include('admin.industrial.borderaux.equipments.edite_equipment')
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function()
    {
    $('[data-toggle="tooltip"]').tooltip();   
    }); 
</script>
@endsection
    

