@extends('layouts.admin')

@section('content')
<style type="text/css"> .center{text-align: center;}</style>
        @if(session()->has('success'))

        <div class="alert alert-success">
          <p>{{session()->get('success')}}</p>
        </div>
        @endif

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i></span>
                   
                    <span>{{ $article->equipment_title }}</span>
                   
                </h3>
            </div>

            <div class="box-body">

                <div class="well well-sm well-toolbar">
                    <a href="{{url('admin/industrial/'.$article->id.'- /sousequipments')}}" class="btn btn-primary" type="button">
                        <span class="btn-label"><i class="fa fa-fw fa-user-plus"></i></span>Ajouter un sous article
                     </a>
                      <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#category" style="float: right;">
                        <span class="btn-label"><i class="fa fa-fw fa-user-plus"></i></span>Ajouter une catégorie
                     </button>
                     @include('admin.industrial.borderaux.equipments.category.add_category',['equip_id'=> $article->id])
                     </div>

                @if(!empty($category[0]))
                <div>
                    <h4 class="box-title">
                        <span><i class="fa fa-table"></i></span>
                        <span>Liste des categorie</span>
                    </h4>
                    <table id="tbl-list3" data-server="false" class="dt-table table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="center">N°</th>
                                <th class="center">Nom</th>
                                <th class="center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($category as $category)
                            <tr>
                                <td width="30px" >{{ $loop->index+1 }}</td>
                                <td>{{$category->category_title}}</td>
                                <td width="140px" class="center">
                                    <a href="{{url('admin/industrial/category'.$category->id.'-'.$article->id.'/sousequipments')}}" class="btn btn-primary" > <i class="fa fa-table" data-toggle="tooltip" title="Détail"></i></a>
                                    <button  class="btn btn-warning " type="button" data-toggle="modal" data-target="#edit_categ" data-id="{{$category->id}}" data-title="{{$category->category_title}}">
                                    <i class="fa fa-pencil-square-o"data-toggle="tooltip" title="Info / Editer"></i></button>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_categ" data-id="{{$category->id}}" ><i class="fa fa-trash-o"data-toggle="tooltip" title="Supprimer"></i></button>
                                 </td>
                            </tr>
                         @endforeach
                        </tbody>
                    </table>
                    @include('admin.industrial.borderaux.equipments.category.destroy_categ')
                    @include('admin.industrial.borderaux.equipments.category.edit_categ')
                </div>
                @endif

                <div >
                    <h4 class="box-title">
                        <span><i class="fa fa-table"></i></span>
                        <span>Liste des equipments</span>
                    </h4>
                <table id="tbl-list1" data-server="false" class="dt-table table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="center">N°</th>
                        <th class="center">Nom</th>
                        <th class="center">Action </th>
                    </tr>
                    </thead>
                    <tbody> 
                     @foreach($equipment as $equipment)
                         <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{$equipment->app_display}}</td>
                            <td width="130px">
                            <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#edit" data-id="{{$equipment->id}}" data-title="{{$equipment->sousequi_title}}" data-description="{{$equipment->sousequi_description}}" data-unit="{{$equipment->sousequi_unit}}" data-abre="{{$equipment->sousequi_unit_abre}}"
                                data-four="{{$equipment->four_price}}" data-pose="{{$equipment->pose_price}}" data-dup="{{$equipment->duplicate}}"
                                data-desc="{{$equipment->descrip}}" data-pm="{{$equipment->pm}}" data-app="{{$equipment->app_display}}" >
                            <i class="fa fa-pencil-square-o"data-toggle="tooltip" title="Editer"></i></button>  
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete"><i class="fa fa-trash-o" data-toggle="tooltip" title="Supprimer"></i></button> 
                            @include('admin.industrial.borderaux.sousequip.destroy_sousequi')    
                                
                             </td>
                         </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>
            @include('admin.industrial.borderaux.sousequip.edite_sousequi')
        </div>
    </div>
    @endsection
    
<script type="text/javascript">
     $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
}); 
</script>
