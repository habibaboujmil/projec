@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
               
            </div>
          <div class="box-body">
       
    <form action="{{url('admin/industrial/sousequipments')}}" method="post">
    {{csrf_field()}}  
      <input type="HIDDEN" name="equi_id" class="form-control" value="{{$equip_id}}"  >
      
              
                <h5 style="margin: 35px 20px; color: #22427C;" >
                  <span style="color: red; font-size: 24px">*</span>
                  <b>Les données renseignées ci-dessous seront affichées dans </b>
                </h5>
              
              <div class="form-group " >
                @if(isset($category_id))
                <input type="HIDDEN" name="category_id" class="form-control" value="{{ $category_id }}">
                @else <input type="HIDDEN" name="category_id" class="form-control" >
                @endif
              </div>
              <div class="form-group marg_div">
                  <label for="app_display">Titre </label>
                  <input type="text" name="app_display" class="form-control">
              </div>
              <div class="form-group">
                <div class="col col-6">
                  <label for="sousequi_unit">Unité Complète </label>
                  <input type="text" name="sousequi_unit" class="form-control"  >
                </div>
                <div class="col col-6">
                  <label for="sousequi_unit">Unité en abréviation</label>
                  <input type="text" name="sousequi_unit_abre" class="form-control"  >
                </div>
              </div>
              <div>
          <div class="col col-6" style="margin-top: 15px ">
            <input type="checkbox" name="duplicate" value="1" >Autoriser la duplication
          </div>
          <div class="col col-6" style="margin-top: 15px ">        
            <input type="checkbox" name="descrip" value="1" > Autoriser l'ajout d'un texte
          </div>
        </div>
            </div>
          




          <!-- form BORD data -->
          
            <div style="height:80px">
              <h5 style="margin:16px; color: #22427C;">
                <span style="color: red; font-size: 24px">*</span>
                <b>Les données renseignées ci-dessous seront utilisées pour générer les bordereaux</b>
              </h5>
            </div>
            <div class="form-group marg_div">
              <label for="equipment_title">Titre </label>
              <input type="text" name="sousequi_title" class="form-control" value="{{old('equipment_title')}}" >
            </div>
              <div class="form-group marg_div" >
                <label for="equipment_description">Designation de l'ouvrage</label>
                <textarea name="sousequi_description" class="form-control" rows="6" cols="33" >
                </textarea>
            </div>
            <div class="marg_div form-group">
              <div class="col col-6">
                <label for="four_price">Prix de la Fourniture </label>
                <input type="text" name="four_price" class="form-control" >
              </div>
              <div class="col col-6">
                <label for="pose_price">Prix de la Pose </label>
                <input type="text" name="pose_price" class="form-control" >
              </div>
            </div>
          <div >
            <input class="btn btn-primary sub_btn"  type="submit" name="sous_equip" value="suivant">
          </div>
      </form>
      
        
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .sub_btn{float: right;}
  .marg_div{margin: 16px 16px;}
</style>
@endsection
