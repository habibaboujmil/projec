@extends('layouts.admin')
@section('content')
<style type="text/css"> .center{text-align: center;}</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i></span>
                   
                    <span>{{ $category->category_title }}</span>
                   
                </h3>
            </div>

            <div class="box-body">
                <div class="well well-sm well-toolbar">
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#create">
                        <span class="btn-label"><i class="fa fa-fw fa-user-plus"></i></span>Ajouter un sous article
                     </button>
                      @include('admin.industrial.borderaux.sousequip.add_sousequi')
                     
                </div>
                <div>
                    <h4 class="box-title">
                        <span><i class="fa fa-table"></i></span>
                        <span>Liste des sous articles</span>
                    </h4>
                    <table id="tbl-list" data-server="false" data-page-length="25" class="dt-table table nowrap table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="center">N°</th>
                            <th class="center">Nom</th>
                            <th class="center">Description</th>
                            <th class="center">Unité</th>
                            <th class="center">Prix<br>fourniture </th>
                            <th class="center">Prix<br>pose </th>
                            <th class="center">Dup-<br>lication</th>
                            <th class="center">Ajout<br>texte </th>
                            <th class="center">Action </th>
                        </tr>
                        </thead>
                        <tbody> 
                         @foreach($equipment as $equipment) 
                             <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{$equipment->sousequi_title}}</td>
                                <td>{{$equipment->sousequi_description}}</td>
                                <td>{{$equipment->sousequi_unit}}</td>
                                <td>{{$equipment->four_price}}</td>
                                <td>{{$equipment->pose_price}}</td> 
                                <td></td>
                                <td></td>
                                <td width="140px" class="center">
                                    <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#edit" data-id="{{$equipment->id}}" data-title="{{$equipment->sousequi_title}}" data-description="{{$equipment->sousequi_description}}" data-unit="{{$equipment->sousequi_unit}}" data-abre="{{$equipment->sousequi_unit_abre}}"
                                    data-four="{{$equipment->four_price}}" data-pose="{{$equipment->pose_price}}" data-dup="{{$equipment->duplicate}}"
                                    data-desc="{{$equipment->descrip}}" >
                                    <i class="fa fa-pencil-square-o"data-toggle="tooltip" title="Editer"></i></button>
                                    
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete" data-id="{{$equipment->id}}" ><i class="fa fa-trash-o"data-toggle="tooltip" title="Supprimer"></i></button> 
                                    @include('admin.industrial.borderaux.sousequip.destroy_sousequi') 
                                 </td>
                            </tr>
                            @endforeach  
                        </tbody>
                    </table>
                </div>
                 @include('admin.industrial.borderaux.sousequip.edite_sousequi')
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function()
    {
    $('[data-toggle="tooltip"]').tooltip();   
    }); 
</script>
@endsection
    

