@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
          <div class="well well-sm well-toolbar menu">
            <i class="fa fa-plus" onclick="duplicateLevel1('Level1')"></i>
            <strong >Niveau 1</strong>
            <i class="fa fa-plus" onclick="duplicateLevel2('Level2')"></i>
            <strong >Niveau 1.1</strong>
            <i class="fa fa-plus" onclick="duplicateLevel3('Level3')"></i>
            <strong >Niveau 1.1.a</strong>
          </div>
          <div class="box-body">
           <form @if($indication=='equipment')
                 action="{{url('admin/industrial/equipments')}}"
                 @elseif($indication=='sous_equip')
                 action="{{url('admin/industrial/sousequipments')}}"
                 @elseif($indication=='article')
                 action="{{url('admin/industrial/articles')}}"
                 @endif 
           
                 method="post" enctype="multipart/form-data">
                 
            {{csrf_field()}}
              <div class="form-group ">
                <div id="Level1">
                  <input type="hidden" id="test0" name="test0" value="0">
                  <label for="l1_title">1. Titre </label>
                  <input type="text" name="l1_title[]" class="form-control" value="{{old('l1_title')}}" >
                  <label for="l1_clause">Clause</label>
                  <textarea name="l1_clause[]" class="form-control" value="{{old('l1_clause')}}" onmouseup="auto_grow(this)">
                  </textarea>
                  <input type="file" name="l1_file[]" class="file">
                </div>


                <input type="hidden" id="test1" name="test1" value="0">
                <div class="divclone_l2 "  id="Level2" >
                  <label for="l2_title">1.1:  Titre </label>
                  <input id="l2_title" type="text" name="l2_title[0][]" class="form-control" >
                  <label for="l2_clause">Clause</label>
                  <textarea id="l2_clause" name="l2_clause[0][]" class="form-control" onmouseup="auto_grow(this)">
                  </textarea> 
                  <input id="l2_file" type="file" name="l2_file[0][]" class="file">
                </div> 

                <div class="divclone_l3"  id="Level3">
                    <label for="l3_title[]">1.1.a:  Titre </label>
                    <input id="l3_title"  type="text" name="l3_title[0][0][]" class="form-control">
                    <label for="l3_clause[]">Clause</label>
                    <textarea id="l3_clause" name="l3_clause[0][0][]" class="form-control" value="{{old('l1_clause
                    ')}}" onmouseup="auto_grow(this)">
                    </textarea>
                    <input id="l3_file" type="file" name="l3_file[0][0][]" class="file">
                  </div> 
            </div>
            <div>
              <input class="btn btn-primary" type="submit" name="clause" value="sauvgarder">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
  var test0=1; 
  var incr=1;
  var test1=1;
  var add=1;

function duplicateLevel1(i)
  {
    test1= document.getElementById('test1').value = 0;

    document.getElementById('test0').value = test0++;
    
    var original = document.getElementById(i);
    var clone = original.cloneNode(true);
    original.parentNode.appendChild(clone);
  }

  function duplicateLevel2(i)
  {
    
    index0 = document.getElementById('test0').value ;
    
    var original = document.getElementById(i);
    var clone = original.cloneNode(true);
    clone.getElementsByTagName('input')[0].setAttribute('id', 'l2_title' +incr);
    clone.getElementsByTagName('input')[1].setAttribute('id', 'l2_file' +incr);
    clone.getElementsByTagName('textarea')[0].setAttribute('id', 'l2_clause' +incr);
    original.parentNode.appendChild(clone);
     document.getElementById('l2_title'+ incr).setAttribute('name', 'l2_title['+index0+'][]');
     document.getElementById('l2_clause'+ incr).setAttribute('name', 'l2_clause['+index0+'][]');
     document.getElementById('l2_file'+ incr).setAttribute('name', 'l2_file['+index0+'][]');
     incr++;
     document.getElementById('test1').value = test1++;
  }

  function duplicateLevel3(i)
  {
    index0 = document.getElementById('test0').value ;
    index1 = document.getElementById('test1').value ;

    var original = document.getElementById(i);
    var clone = original.cloneNode(true);
    clone.getElementsByTagName('input')[0].setAttribute('id', 'l3_title'+add);
    clone.getElementsByTagName('input')[1].setAttribute('id', 'l3_file'+add);
    clone.getElementsByTagName('textarea')[0].setAttribute('id', 'l3_clause'+add);
    original.parentNode.appendChild(clone);
     
     document.getElementById('l3_title'+add).setAttribute('name', 'l3_title['+index0+']['+index1+'][]');
     document.getElementById('l3_clause'+add).setAttribute('name', 'l3_clause['+index0+']['+index1+'][]');
     
     add++;
  }

function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}


// Create a clone of the menu, right next to original.
$('.menu').addClass('original').clone().insertAfter('.menu').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();

scrollIntervalID = setInterval(stickIt, 10);


function stickIt() {

  var orgElementPos = $('.original').offset();
  orgElementTop = orgElementPos.top;               

  if ($(window).scrollTop() >= (orgElementTop)) {
    // scrolled past the original position; now only show the cloned, sticky element.

    // Cloned element should always have same left position and width as original element.     
    orgElement = $('.original');
    coordsOrgElement = orgElement.offset();
    leftOrgElement = coordsOrgElement.left;  
    widthOrgElement = orgElement.css('width');
    $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
    $('.original').css('visibility','hidden');
  } else {
    // not scrolled past the menu; only show the original menu.
    $('.cloned').hide();
    $('.original').css('visibility','visible');
  }
}
</script>
<style type="text/css">
  .divclone_l2{margin-left: 30px; margin-top: 20px; display: block;}
  .divclone_l3{margin-left: 60px;margin-top: 20px; display: block;}
  .file{float: right;cursor: pointer;}
  i{margin-left: 20px}
</style>
          @endsection
     
  