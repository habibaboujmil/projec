@extends('layouts.admin')

@section('content')

        @if(session()->has('success'))

        <div class="alert alert-success">
          <p>{{session()->get('success')}}</p>
        </div>
        @endif

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i></span>
                   
                    <span>{{ $building->type }}</span>
                   
                </h3>
            </div>

            <div class="box-body">

                <div class="well well-sm well-toolbar">
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#create">
                        <span class="btn-label"><i class="fa fa-fw fa-plus"></i></span>Nouveau Projet
                     </button>
                     @include('admin.industrial.borderaux.project.add')
                     </div>
                </div>
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <span><i class="fa fa-table"></i></span>
                        <span>Liste des Projets</span>
                    </h3>
                </div>
                <table id="tbl-list" data-server="false" class="dt-table table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>N°</th>
                        <th>Projet</th>
                        <th>réference</th>
                        <th>Maitre d'ouvrage</th>
                        <th>Action </th>
                    </tr>
                    </thead>
                    <tbody> 
                     @foreach($project as $project)
                         <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{$project->name}}</td>
                            <td>{{$project->reference}}</td>
                            <td>{{$project->ouvrage}}</td>
                            <td width="130px">
                                <a href="{{url('admin/industrial/project/lot/'.$project->id)}}" class="btn btn-primary" > <i class="fa fa-table" data-toggle="tooltip" title="Détail"></i></a>
                                <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#edit" data-id="{{$project->id}}" data-name="{{$project->name}}" data-reference="{{$project->reference}}" data-ouvrage="{{$project->ouvrage}}" ><i class="fa fa-pencil-square-o" data-toggle="tooltip" title="Editer"></i></button>
                               
                             </td>
                         </tr>
                         @endforeach
                    </tbody>
                    @include('admin.industrial.borderaux.project.edit')
                </table>
            </div>
        </div>
    </div>
@if ($errors-> has('add_reference') or $errors-> has('add_name') )
    <script>
        $( document ).ready(function() {
            $('#create').modal('show');
        });
    </script>
@endif
@if ($errors-> has('reference') or $errors-> has('name') )
    <script>
        $( document ).ready(function() {
            $('#edit').modal('show');
        });
    </script>
@endif


<script type="text/javascript">
     $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
}); 
</script>
    @endsection
 