 <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" align="center"><b> Editer </b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/industrial/projects/edit')}}" method="post" id="edit">
                    <input type="hidden" name="_method" value="PUT">
                {{csrf_field()}}
            <input type="hidden" name="project_id" id="id" class="form-control">
                <div class="form-group has-feedback">
                    <label for="name">Nom de Projet </label>
                    <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}">
                    @if ($errors-> has('name'))
                      <i class="fa fa-times-circle" style="color: red;" >
                        Ce champ est déjà pris!  
                      </i>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <label for="reference">Référence</label>
                    <input type="text" name="reference" class="form-control" id="reference"@if ($errors-> has('reference')) value="{{old('reference') }}"
                    @endif >
                    @if ($errors-> has('reference'))
                      <i class="fa fa-times-circle" style="color: red;" >
                        Ce champ est déjà pris!  
                      </i>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <label for="ouvrage">Maitre d'ouvrage</label>
                    <input type="text" name="ouvrage" class="form-control" id="ouvrage" >
                    
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
                    <input id="submitForm"  class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="submit" name="form-control" value="sauvgarder">
                </div>
                </form>
            </div>
                <div class="modal fade" id="alert_modal" style="margin-top: 150px;">
        <div class="modal-body">

            <h3>Alert</h3>

            <h5 id="alert_message"></h5>

            <div class="row-fluid">
                <div class="span12">
                    <button type="button" class="btn btn-danger span2" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
        </div>
    </div>
</div>
@if(isset($errors))
@section('scripts')
    
        <script type="text/javascript">
         $('#edit').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var name = button.data('name') 
      var reference = button.data('reference')
      var ouvrage= button.data('ouvrage') 
       
      var id = button.data('id') 

      var modal = $(this)
      modal.find('.modal-body #name').val(name);
      modal.find('.modal-body #reference').val(reference);
      modal.find('.modal-body #ouvrage').val(ouvrage);
      
      modal.find('.modal-body #id').val(id);
    })
    </script>
@endsection
@endif
