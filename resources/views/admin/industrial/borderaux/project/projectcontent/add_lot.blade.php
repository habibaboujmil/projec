<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h5><br>Nouveau Lot</h5>
      </div>
      <div class="modal-body">
        <form action="{{url('admin/industrial/project/lot')}}" method="post">
        {{csrf_field()}}
          <div class="form-group ">
          <input type="HIDDEN" name="project_id" class="form-control" value="{{ $project->id }}"  >
          </div>
          <div class="form-group ">
          <label for="lot">Choisissez un lot  </label>
             @foreach($lot_list as $lot_list)
             <div class="row-md-12">
             <input type="radio" value="{{$lot_list->id}}" name="lot">{{$lot_list->lot_name}}

            </div>
            <div class="form-group ">
         
          </div>
            @endforeach
       
         
          <div >
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
        <input class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="submit" name="form-control" value="sauvgarder">
        </div>
      </form>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>