@extends('layouts.admin')

@section('content')
<form action="{{url('admin/industrial/save')}}" method="post">
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span class="lienAfficher"></i></span>
                   <span >{{ $project_name->name }} / {{ $lot_name->lot_name }} </span>
                </h3>
                <!-- project reset buttum -->
                <button style="float: right;background:black; border: none;" type="submit" name="submit" value="reset"><i class="fa fa-refresh" aria-hidden="true"data-toggle="tooltip" title="Reset"></i>
                </button>  
            </div >
          
            <div>
                <input type="hidden" name="lot" value="{{ $lot_name->id }}">
                <input type="hidden" name="project_id" value="{{ $project_name->id }}">
                <input type="hidden" name="project" value="{{ $project_name->name }}">
                <input type="hidden" name="reference" value="{{ $project_name->reference}}">
                {{csrf_field()}}
                 @if($errors->has('article'))      
                    <div class="alert" role="alert">
                       <i class="fa fa-times-circle" style="color: red; font-size: 16px" >
                        <b>Minimum un article doit être coché !</b>  
                      </i>
                    </div>
                @endif   
                <input type="hidden" value="{{$i=0}}">

            @foreach($articles as $article)

            <div class="panel panel-default">
                <div class="panel-heading" >
                    <table width="100%">
                        <thead>
                            <tr>
                                <th width="30px">
                                <input data-toggle="collapse" data-target="#{{$loop->index}}" onclick="afficherMasquer('qu{{$loop->index}}')"type="checkbox" name="article[]" value="{{$article->id}}"
                                 @if (isset ($article_data)) {{ in_array($article->id,  $article_data) ? 'checked' : '' }}@endif
                                 {{ in_array($article->id, old('article', [])) ? 'checked' : '' }} >
                                </th>
                                <th onclick="afficherMasquer('qu{{$loop->index}}')" data-toggle="collapse" data-target="#{{$loop->index}}">{{$article->article_title}}
                                </th>
                                <th width="200px" style="display: none; float: right;margin-right: 8px; text-align: center;"  id="qu{{$loop->index}}">
                                 &nbsp &nbsp Quantité &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                 <span data-toggle="tooltip" title="Fourniture">F</span>  &nbsp 
                                 <span data-toggle="tooltip" title="Pose"> P </span>  PM 
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="collapse" id="{{$loop->index}}" >
                    <div class="panel-body">
                    <p style="display: none;">{{$equipments=App\equipment::equip_of_article($article->id)}}</p>
                    @foreach($equipments as $key=> $equi)
                    @if($article->level == 1)
                        @include('admin.industrial.borderaux.project.partials.panel_sousequip', ['collapse_id' =>$article->id -1])
                        
                    @else
                    <table width="100%">
                        <tbody>
                         @include('admin.industrial.borderaux.project.partials.panel_equip', ['collapse_id' =>$article->id -1])

                        </tbody>
                    </table>
                    @endif     
                    @endforeach
                    </div>
                </div>
            </div>
            @endforeach

            <div class="dropdown" style="float: right; margin:20px">
                <button class="btn btn-primary"  style="margin-left:20px" type="submit" name="submit" value="Enregistrer"><span><i class="fa fa-save">  Enregistrer</i></span>
                </button>
                @if(isset ($article_data))
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="margin-left:20px;margin-right:60px"><span><i class="fa fa-download">  Exporter</i></span>
                </button>
                <ul class="dropdown-menu" style="margin-bottom:20px;margin-left:145px">
                    <li>
                        <a href="{{url('admin/industrial/exportation/borderaux/'.$project_name->id .$lot_name->id )}}" target="_blank">Borderaux</a>
                    </li>
                    <li>
                        <a href="{{url('admin/industrial/exportation/estimation/'.$project_name->id .$lot_name->id )}}" target="_blank">Estimation</a>
                    </li>
                    <li>
                        <a href="{{url('admin/industrial/exportation/CPTP/'.$project_name->id .$lot_name->id )}}" target="_blank">CPTP</a>
                    </li>
                </ul>
                @endif
            </div>
            </div>
             
        </form>
        </div>
   </div>
</div>




<style type="text/css">
    .sub{
        background:transparent;
        border:0px;
        }
        td{padding-top:2px; padding-bottom:2px }
        th {padding-top:2px; padding-bottom:2px }
</style>
 
    <script type="text/javascript">
     $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
}); 

 $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});   

function duplicate(i) {
    var original = document.getElementById(i);
    var clone = original.cloneNode(true);
    original.parentNode.appendChild(clone);
}

function afficherMasquer(id)
{
  if(document.getElementById(id).style.display == "none")
    document.getElementById(id).style.display = "block";
  else
    document.getElementById(id).style.display = "none";
}

function change (iconID)
{
    if(document.getElementById(iconID).className=="fa fa-chevron-right"){
    document.getElementById(iconID).className = "fa fa-chevron-down";
    }
    else{document.getElementById(iconID).className = "fa fa-chevron-right";}
}

function changeborder (ID)
    {
        if(document.getElementById(ID).style.borderColor !="blue")
            { document.getElementById(ID).style.borderColor ="blue";}
        else{document.getElementById(ID).style.borderColor ="gainsboro";}

    }

function errors_quantite (quantite,collapse_id,cross,categ_collapse,sous_equip_collapse,old_quan)

{ 
   document.getElementById(collapse_id).className ="collapse in";
   document.getElementById('qu' + collapse_id).style.display = "block";
  
   document.getElementById(cross).className ="fa fa-times-circle";
   document.getElementById(cross).style.color="red"; 
   document.getElementById(quantite).style.borderColor ="red";
  if (sous_equip_collapse !='') {document.getElementById('ch' +sous_equip_collapse).className ="collapse in";}
   if (categ_collapse !='') {document.getElementById(categ_collapse).className ="collapse in";}
   
  
                           
}

function errors_equip (quantite,collapse_id,cross,categ_collapse,sous_equip_collapse,categ_sequip_collapse)
{
   document.getElementById(collapse_id).className ="collapse in";
   document.getElementById('qu' + collapse_id).style.display = "block";
   document.getElementById(cross).className ="fa fa-times-circle";
   document.getElementById(cross).style.color="red";
   document.getElementById(quantite).style.borderColor ="red";
   if (sous_equip_collapse !='') {document.getElementById('ch'+sous_equip_collapse).className ="collapse in";}

   if (categ_collapse !='') {document.getElementById(categ_collapse).className ="collapse in";}                          
}

</script>

@endsection

