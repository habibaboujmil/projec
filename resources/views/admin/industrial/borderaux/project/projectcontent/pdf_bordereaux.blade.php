<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bordereaux</title>
</head>
<body>
<div id="header_bord">
	<div style="float: left; display:inline; margin-bottom: 10px" >
		<p style="float:left;margin-top: 10px">Bordereaux de prix <br>{{$lot_info->tot_name}} </p>
		<p style="float:right;margin-top: 10px">{{$project}}</p>
	</div>

</div>
@include('admin.industrial.borderaux.project.partials.bord_pdf.header_footer')
@include('admin.industrial.borderaux.project.partials.spicification_bord')
@include('admin.industrial.borderaux.project.partials.bord_pdf.recap_tab')
<div style=" width : 730px; height :924px; margin-top:30px;" class="page_break">
<table  style="margin-top: 10px;">
	<thead>
		<tr class="center">
    		<th  width="20px">N°Des Prix</th>  
        	<th  width="400px">DESIGNATION DES OUVRAGES</th> 
			<th  width="20px">Unité</th>
        	<th  width="30px">Quan<br>tité</th>
        	<th  width="30px">Prix<br> Unitaire <br>HTVA</th>
			<th  width="30px">Totaux <br>Partiels<br> HTVA</th> 
	  	</tr>
	</thead>
	<tbody>
		{{$i=0}}
		{{$N='A'}}
		{{$k=0}}
		{{$quan=0}}
		{{$sp=0}} <!--compteur pour champ spcial-->
		{{$sousp=0}} <!--compteur pour champ souspcial-->
		{{$quanlev=0}} <!--compteur pour  quantite: cas ou article 2 niveau / equip n'a pas un sous equip-->
		@foreach ($articles as $article)
		 <tr>
	        <td class="center">{{$N}}</td>
	        <td><strong>{{$article->article_title}}</strong><br>{!!nl2br(e($article->article_description))!!}</td>
	        <td></td>
	        <td></td>
	        <td></td>
	        <td></td>
	    </tr>

	    {{$j=1}}
	    @while ($equipments[$i]->article_id == $article->id)
	    @if($article->level==1)
	    @include('admin.industrial.borderaux.project.partials.bord_pdf.sous_equip_list')
	    {{$quanlev++}}
	    @else
	    
	    <tr>
	       	<td class="center">{{$j}}</td>
	       	<td>{{$equipments[$i]->equipment_title}} <br> {{$equipments[$i]->equipment_description}} 
	       	@if(!empty($spcial[$equipments[$i]->id]))
	       	{{$spcial[$equipments[$i]->id][$sp]}}
	       	</td>
	       	@if($sp < count($spcial[$equipments[$i]->id])-1){{$sp++}}	@else {{$sp=0}}@endif
	       	@endif
	       	<td></td>
	       	<td></td>
	       	<td></td>
	       	<td></td>
	       	
	    </tr>
	    @if(!empty($four[$equipments[$i]->id]))
	     <tr>
	        <td class="center">a</td>
	        <td >La Fourniture en {{$equipments[$i]->equipment_unit}}</td>
	        <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
	        <td class="center">{{$quantite[$equipments[$i]->id][0]}}</td>
	        <td></td>
	        @if(!empty($pm[$equipments[$i]->id]))
	        <td>PM</td>
	        @else <td></td>
	        @endif
	    </tr>
	    @elseif(!empty($pose[$equipments[$i]->id]))
	    <tr>
	        <td class="center">a</td>
	        <td >La Pose en {{$equipments[$i]->equipment_unit}}</td>
	        <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
	        <td class="center">{{$quantite[$equipments[$i]->id][0]}}</td>
	        <td></td>
	        @if(!empty($pm[$equipments[$i]->id]))
	        <td>PM</td>
	        @else <td></td>
	        @endif
	    </tr>
	    @else
        <tr>
	        <td class="center">a</td>
	        <td >La Fourniture en {{$equipments[$i]->equipment_unit}}</td>
	        <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
	        <td class="center">{{$quantite[$equipments[$i]->id][0]}}</td>
	        <td></td>
	        @if(!empty($pm[$equipments[$i]->id]))
	        <td>PM</td>
	        @else <td></td>
	        @endif
	        </tr>
	        <tr>
	        <td class="center">b</td>
	        <td >La Pose en {{$equipments[$i]->equipment_unit}}</td>
	        <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
	        <td class="center">{{$quantite[$equipments[$i]->id][0]}}</td>
	        <td></td>
	        @if(!empty($pm[$equipments[$i]->id]))
	        <td>PM</td>
	        @else <td></td>
	        @endif
	    </tr>
	    @endif
	    {{$quan++}}
        @endif
	    {{$j++}}
	    @if ($i==$break-1)
        @break
        @endif
        {{$i++}}
	    @endwhile
         <tr>
		    <td></td>
		    <td><strong> TOTAL ARTICLE  {{$N}}</strong></td>
		    <td></td>
		    <td></td>
		    <td></td>
		    <td></td>
	    </tr>
	    {{$N++}}
		 @endforeach
	
	</tbody>
	     		
</table>
</div>
</body>
</html>




