@extends('layouts.admin')

@section('content')

        @if(session()->has('success'))

        <div class="alert alert-success">
          <p>{{session()->get('success')}}</p>
        </div>
        @endif

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i></span>
                   
                    <span>{{ $project->name }}</span>
                   
                </h3>
            </div>

            <div class="box-body">

                <div class="well well-sm well-toolbar">
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#create">
                        <span class="btn-label"><i class="fa fa-fw fa-user-plus"></i></span>Ajouter un Lot
                     </button>
                     @include('admin.industrial.borderaux.project.projectcontent.add_lot')
                </div>
                </div>
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <span><i class="fa fa-table"></i></span>
                        <span>Liste des lots</span>
                    </h3>
                <table id="tbl-list" data-server="false" class="dt-table table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>N°</th>
                        <th>Nom du Lot</th>
                        <th>Modifié le</th>
                        <th>Modifié par</th>
                        <th>Action </th>
                    </tr>
                    </thead>
                    <tbody> 
                        @foreach($projectlot as $projectlot)
                         <tr>
                            <td width="30px">{{ $loop->index+1 }}</td>
                            <td>{{$projectlot->lot}}</td>
                            <td>{{$projectlot->updated_at}}</td>
                            <td>{{$projectlot->updated_by}}</td>
                            <td width="30px">
                                <a href="{{url('admin/industrial/project/bord/'.$projectlot->lot_id.'/'.$project->id)}}" class="btn btn-primary" > <i class="fa fa-table"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
    @endsection
    

