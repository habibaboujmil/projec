<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Estimation de prix</title>
</head>
<body>
<div id="header_estim"  >
	<div style="float: left; display:inline; margin-bottom: 10px" >
		<p style="float:left;">Estimation de prix <br>{{$lot_info->tot_name}} </p>
		<p style="float:right;margin-top: 10px">{{$project}}</p>
	</div>
</div>
@include('admin.industrial.borderaux.project.partials.bord_pdf.header_footer')
@include('admin.industrial.borderaux.project.partials.spicification_bord')
@include('admin.industrial.borderaux.project.partials.bord_pdf.recap_tab')



<table style="margin-top: 10px;">
	<thead>
		<tr class="center">
    		<th  width="20px">N°Des Prix</th>  
        	<th  width="360px">DESIGNATION DES OUVRAGES</th> 
			<th  width="20px">Unité</th>
        	<th  width="30px">Quan<br>tité</th>
        	<th  width="50px">Prix<br> Unitaire <br>HTVA</th>
			<th  width="70px">Totaux <br>Partiels<br> HTVA</th> 
	  	</tr>
	</thead>
	<tbody>
		{{$i=0}}
		{{$N='A'}}
		
		{{$quan=0}}
		{{$sp=0}} <!--compteur pour champ spcial-->
		{{$sousp=0}} <!--compteur pour champ souspcial-->
		@foreach ($articles as $article)
		{{$tot_prix=0}}
		 <tr>
	        <td class="center">{{$N}}</td>
	        <td><strong>{{$article->article_title}}</strong><br>{{$article->article_description}}</td>
	        <td></td>
	        <td></td>
	        <td></td>
	        <td></td>
	    </tr>

	    {{$j=1}}
	    @while ($equipments[$i]->article_id == $article->id)
	    @if($article->level==1)
	     @if($equipments[$i]->has_sousequip == 1)
	     {{$k=0}}
	    <tr>
	       	<td class="center">{{$j}}</td>
	       	<td>
	       		@if(isset($equipments[$i]->equipment_title))
	       		<strong>{{$equipments[$i]->equipment_title}}</strong><br>
	       		@endif 
	       	    {{$equipments[$i]->equipment_description}} 
	       	</td>
	       	<td></td>
	       	<td></td>
	       	<td></td>
	       	<td></td>  	
	    </tr>
	    
	    @foreach ($sousequip as  $sousequi)
	    @if ($sousequi->equi_id == $equipments[$i]->id )
        <tr>
	       	<td class="center">{{$j}}.{{$k+1}}</td>
	       	<td>
	       		@ifisset($sousequi->sousequi_title))
	       		{{$sousequi->sousequi_title}}
	       		 {{$sousequi->sousequi_description}} 
	       	@if(!empty($souspcial[$sousequi->id]))
	       	{{$souspcial[$sousequi->id][0]}}	 
	       	</td>
	       	@endif
	       	<td></td>
	       	<td></td>
	       	<td></td>
	       	
	       	<td></td>  	
	    </tr>  	
	       	
	    @if(!empty($sousfour[$sousequi->id]))
	    <tr>
	        <td class="center">a</td>
	        <td >La Fourniture en {{$sousequi->sousequi_unit}}</td>
	        <td class="center">{{$sousequi->sousequi_unit_abre}}</td>
	        <td class="center">{{$sousq=$sousquan[$sousequi->id][0]}}</td>
	        <td>{{$sousf=number_format($sousequi->four_price,3)}}</td>
	        {{$tot_sousf=$sousf*$sousq}}
	        <td>{{$tot_sousf}}</td>
	       {{$tot_prix+=$tot_sousf}}
	    </tr>
	    {{$k++}}
	    @elseif(!empty($souspose[$sousequi->id]))
	    <tr>
	        <td class="center">a</td>
	        <td >La Pose en {{$sousequi->sousequi_unit}}</td>
	        <td class="center">{{$sousequi->sousequi_unit_abre}}</td>
	        <td class="center">{{$sousq=$sousquan[$sousequi->id][0]}}</td>
	        <td>{{$sousp=number_format($sousequi->pose_price,3)}}</td>
	        {{$tot_sousp=$sousp*$sousq}}
	        <td>{{$tot_sousp}}</td>
	        {{$tot_prix+=$tot_sousp}}
	    </tr>
	    {{$k++}}
	    @else
        <tr>
	        <td class="center">a</td>
	        <td >La Fourniture en {{$sousequi->sousequi_unit}}</td>
	        <td class="center">{{$sousequi->sousequi_unit_abre}}</td>
	        <td class="center">{{$sousq=$sousquan[$sousequi->id][0]}}</td>
	        <td>{{$sousf=number_format($sousequi->four_price,3)}}</td>
	        {{$tot_sousf=$sousf*$sousq}}
	        <td>{{$tot_sousf}}</td>
	        </tr>
	        <tr>
	        <td class="center">b</td>
	        <td >La Pose en {{$sousequi->sousequi_unit}}</td>
	        <td class="center">{{$sousequi->sousequi_unit_abre}}</td>
	        <td class="center">{{$sousq=$sousquan[$sousequi->id][0]}}</td>
	        <td>{{$sousp=number_format($sousequi->pose_price,3)}}</td>
	        {{$tot_sousp=$sousp*$sousq}}
	        <td>{{$tot_sousp}}</td>
	        {{$tot_prix+=$tot_sousp+$tot_sousf}}
	        {{$k++}}
	    </tr>
	    
	    @endif
        @endif
        @endforeach
<!-- traitement dans le cas ou article à 2 niveau/ equip à un seul nieau-->       
@else
<tr>
   	<td class="center">{{$j}}</td>
   	<td>
   		@if(isset($equipments[$i]->equipment_title))
	    <strong>{{$equipments[$i]->equipment_title}}</strong><br>
	    @endif
	    {{$equipments[$i]->equipment_description}} 
   	    @if(!empty($spcial[$equipments[$i]->id]))
   	    {{$spcial[$equipments[$i]->id][$sp]}}	 
   	</td>
   	@if($sp < count($spcial[$equipments[$i]->id])-1){{$sp++}}	@else {{$sp=0}}@endif
	
   	@endif
   	<td></td>
   	<td></td>
   	<td></td>
   	
   	<td></td>
   	
</tr>

@if(!empty($four[$equipments[$i]->id]))
<tr>
    <td class="center">a</td>
    <td >La Fourniture en {{$equipments[$i]->equipment_unit}}</td>
    <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
    <td class="center">{{$quan=$quantite[$equipments[$i]->id][0]}}</td>
	<td>{{$four=number_format($equipments[$i]->four_price,3)}}</td>
	{{$tot_four=$four*$quan}}
	<td>{{number_format($tot_four,3)}}</td>
	{{$tot_prix+=$tot_four}}
</tr>
@elseif(!empty($pose[$equipments[$i]->id]))
<tr>
    <td class="center">a</td>
    <td >La Pose en {{$equipments[$i]->equipment_unit}}</td>
    <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
    <td class="center">{{$quan=$quantite[$equipments[$i]->id][0]}}</td>
	<td>{{$pose=number_format($equipments[$i]->pose_price,3)}}</td>
	{{$tot_pose=$pose*$quan}}
	<td>{{number_format($tot_pose,3)}}</td>
    {{$tot_prix+=$tot_pose}}
</tr>
@else
<tr>
    <td class="center">a</td>
    <td >La Fourniture en {{$equipments[$i]->equipment_unit}}</td>
    <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
    <td class="center">{{$quan=$quantite[$equipments[$i]->id][0]}}</td>
	<td>{{$four=number_format($equipments[$i]->four_price,3)}}</td>
	{{$tot_four=$four*$quan}}
	<td>{{number_format($tot_four,3)}}</td>
    
    </tr>
    <tr>
    <td class="center">b</td>
    <td >La Pose en {{$equipments[$i]->equipment_unit}}</td>
    <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
    <td class="center">{{$quan=$quantite[$equipments[$i]->id][0]}}</td>
	<td>{{$pose=number_format($equipments[$i]->pose_price,3)}}</td>
	{{$tot_pose=$pose*$quan}}
	<td>{{number_format($tot_pose,3)}}</td>
    {{$tot_prix+=$tot_pose+$tot_four}}
</tr>
@endif
@endif
<!-- traitement dans le cas ou article à un seul niveau/ equip à un seul nieau-->
	    @else
	    
	    <tr>
	       	<td class="center">{{$j}}</td>
	       	<td>
	       		@if(isset($equipments[$i]->equipment_title))
	       		<strong>{{$equipments[$i]->equipment_title}}</strong><br>
	       		@endif 
	       		{{$equipments[$i]->equipment_description}} 
	       	@if(!empty($spcial[$equipments[$i]->id]))
	       	{{$spcial[$equipments[$i]->id][0]}}	 
	       	</td>
	       	@if($sp < count($spcial[$equipments[$i]->id])-1){{$sp++}}	@else {{$sp=0}}@endif
	       	@endif
	       	<td></td>
	       	<td></td>
	       	<td></td>
	       	<td></td>
	       	
	    </tr>
	    @if(!empty($four[$equipments[$i]->id]))
	     <tr>
	        <td class="center">a</td>
	        <td >La Fourniture en {{$equipments[$i]->equipment_unit}}</td>
	        <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
	        <td class="center">{{$quan=$quantite[$equipments[$i]->id][0]}}</td>
	        <td>{{$four=number_format($equipments[$i]->pose_price,3)}}</td>
	        {{$tot_four=$four*$quan}}
	        <td>{{number_format($tot_four,3)}}</td>
	        {{$tot_prix+=$tot_four}}
	    </tr>
	    @elseif(!empty($pose[$equipments[$i]->id]))
	    <tr>
	        <td class="center">a</td>
	        <td >La Pose en {{$equipments[$i]->equipment_unit}}</td>
	        <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
	        <td class="center">{{$quan=$quantite[$equipments[$i]->id][0]}}</td>
	        <td>{{$pose=number_format($equipments[$i]->pose_price,3)}}</td>
	        {{$tot_pose=$pose*$quan}}
	        <td>{{number_format($tot_pose,3)}}</td>
	        {{$tot_prix+=$tot_pose}}
	    </tr>
	    @else
        <tr>
	        <td class="center">a</td>
	        <td >La Fourniture en {{$equipments[$i]->equipment_unit}}</td>
	        <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
	        <td class="center">{{$quan=$quantite[$equipments[$i]->id][0]}}</td>
	        <td>{{$four=number_format($equipments[$i]->four_price,3)}}</td>
	        {{$tot_four=$four*$quan}}
	        <td>{{number_format($tot_four,3)}}</td>
	        
	        </tr>
	        <tr>
	        <td class="center">b</td>
	        <td >La Pose en {{$equipments[$i]->equipment_unit}}</td>
	        <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
	        <td class="center">{{$quan=$quantite[$equipments[$i]->id][0]}}</td>
	        <td>{{$pose=number_format($equipments[$i]->pose_price,3)}}</td>
	        {{$tot_pose=$pose*$quan}}
	        <td>{{number_format($tot_pose,3)}}</td>
	        {{$tot_prix+=$tot_pose+$tot_four}}
	    </tr>
	    @endif
	    {{$quan++}}
        @endif
	    {{$j++}}
	    @if ($i==$break-1)
        @break
        @endif
        {{$i++}}
        
	    @endwhile
         <tr>
		    <td></td>
		    <td><strong> TOTAL ARTICLE  {{$N}}</strong></td>
		    <td></td>
		    <td></td>
		    <td></td>
		    <td>{{number_format($tot_prix,3,'.',' ')}}</td>
	    </tr>
	    {{$N++}}
		 @endforeach
	
	</tbody>
	     		
</table>
</body>
</html>



