@if($equipments[$i]->has_sousequip == 1)
	    <tr>
	       	<td class="center">{{$j}}</td>
	       	<td><strong>{{$equipments[$i]->equipment_title}}</strong> 
	       	<br>{{$equipments[$i]->equipment_description}} 
	       	</td>
	       	<td></td>
	       	<td></td>
	       	<td></td>
	       	<td></td>  	
	    </tr>
	    
	    @foreach ($sousequip as $key=> $sousequi)
	    @if ($sousequi->equi_id == $equipments[$i]->id )
        <tr>
	       	<td class="center">{{$j}}.{{$k+1}}</td>
	       	<td>{{$sousequi->sousequi_title}}
	       		 {{$sousequi->sousequi_description}} 
	       	@if(!empty($souspcial[$sousequi->id]))
	       	{{$souspcial[$sousequi->id][0]}}	 
	       	</td>
	       	@endif
	       	<td></td>
	       	<td></td>
	       	<td></td>
	       	
	       	<td></td>  	
	    </tr>  	
	       	
	    @if(!empty($sousfour[$sousequi->id]))
	    <tr>
	        <td class="center">a</td>
	        <td >La Fourniture en {{$sousequi->sousequi_unit}}</td>
	        <td class="center">{{$sousequi->sousequi_unit_abre}}</td>
	        <td class="center">{{$sousquan[$sousequi->id][0]}}</td>
	        <td></td>
	        @if(!empty($souspm[$sousequi->id]))
	        <td class="center">PM</td>
	        @else <td></td>
	        @endif
	    </tr>
	    @elseif(!empty($souspose[$sousequi->id]))
	    <tr>
	        <td class="center">a</td>
	        <td >La Pose en {{$sousequi->sousequi_unit}}</td>
	        <td class="center">{{$sousequi->sousequi_unit_abre}}</td>
	        <td class="center">{{$sousquan[$sousequi->id][0]}}</td>
	        <td></td>
	        @if(!empty($souspm[$sousequi->id]))
	        <td class="center">PM</td>
	        @else <td></td>
	        @endif
	    </tr>
	    @else
        <tr>
	        <td class="center">a</td>
	        <td >La Fourniture en {{$sousequi->sousequi_unit}}</td>
	        <td class="center">{{$sousequi->sousequi_unit_abre}}</td>
	        <td class="center">{{$sousquan[$sousequi->id][0]}}</td>
	        <td></td>
	        @if(!empty($souspm[$sousequi->id]))
	        <td class="center">PM</td>
	        @else <td></td>
	        @endif
	        </tr>
	        <tr>
	        <td class="center">b</td>
	        <td >La Pose en {{$sousequi->sousequi_unit}}</td>
	        <td class="center">{{$sousequi->sousequi_unit_abre}}</td>
	        <td class="center">{{$sousquan[$sousequi->id][0]}}</td>
	        <td></td>
	        @if(!empty($souspm[$sousequi->id]))
	        <td class="center">PM</td>
	        @else <td></td>
	        @endif
	    </tr>
	    {{$k++}}
	    @endif
        @endif
        @endforeach
@else

<tr>
   	<td class="center">{{$j}}</td>
   	<td>@if(!empty($equipments[$i]->equipment_title)){{$equipments[$i]->equipment_title}} <br> @endif
    {{$equipments[$i]->equipment_description}} 
   	@if(!empty($spcial[$equipments[$i]->id]))
   	{{$spcial[$equipments[$i]->id][0]}}	 
   	</td>
   	@endif
   	<td></td>
   	<td></td>
   	<td></td>
   	
   	<td></td>
   	
</tr>

@if(!empty($four[$equipments[$i]->id]))
<tr>
    <td class="center">a</td>
    <td >La Fourniture en {{$equipments[$i]->equipment_unit}}</td>
    <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
    <td class="center">{{$quantite[$equipments[$i]->id][0]}}</td>
    <td></td>
    @if(!empty($pm[$equipments[$i]->id]))
    <td class="center">PM</td>
    @else <td></td>
    @endif
</tr>
@elseif(!empty($pose[$equipments[$i]->id]))
<tr>
    <td class="center">a</td>
    <td >La Pose en {{$equipments[$i]->equipment_unit}}</td>
    <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
    <td class="center">{{$quantite[$equipments[$i]->id][0]}}</td>
    <td></td>
    @if(!empty($pm[$equipments[$i]->id]))
    <td class="center">PM</td>
    @else <td></td>
    @endif
</tr>
@else
<tr>
    <td class="center">a</td>
    <td >La Fourniture en {{$equipments[$i]->equipment_unit}}</td>
    <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
    <td class="center">{{$quantite[$equipments[$i]->id][0]}}</td>
    <td></td>
    @if(!empty($pm[$equipments[$i]->id]))
    <td class="center">PM</td>
    @else <td></td>
    @endif
    </tr>
    <tr>
    <td class="center">b</td>
    <td >La Pose en {{$equipments[$i]->equipment_unit}}</td>
    <td class="center">{{$equipments[$i]->equipment_unit_abre}}</td>
    <td class="center">{{$quantite[$equipments[$i]->id][0]}}</td>
    <td></td>
    @if(!empty($pm[$equipments[$i]->id]))
    <td class="center">PM</td>
    @else <td></td>
    @endif
</tr>
@endif
@endif
