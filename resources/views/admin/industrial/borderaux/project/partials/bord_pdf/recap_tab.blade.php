<div class="page_break" style="border: 1px solid; width : 730px; height :924px; margin-top:10px;">
	<h3 class="center">TABLEAU RECAPITULATIF</h3>
	
	<table >
		<thead>
			<tr class="center">
	    		<th  width="40px">N°</th>  
	        	<th  width="450px">DESIGNATION DES OUVRAGES</th> 
				<th  width="220px">MONTANT HORS TAXES</th>
		    </tr>
		</thead>
		<tbody>
			@foreach($total_article as $total)
			<tr>
				<td>{{$loop->index+1}}</td>  
	        	<td>ARTICLE {{$total}}</td> 
				<td></td>
			</tr>
			@endforeach
		</tbody>

	</table >
	<table >
		<thead>
			<tr> 
				<th width="497px" style="background: #A9A9A9;">TOTAL HORS TAXES</th>
				<th width="220px"> </th>
		</tr>	
		</thead>
		<tbody>
			<tr>
				<th style="background: #A9A9A9;">TOTAL REMISE </th>
				<th></th>
			</tr>
			<tr>
				<th style="background: #A9A9A9;">TOTAL HORS TAXES APRES REMISE</th>
				<th></th>
			</tr>
			<tr>
				<th style="background: #A9A9A9;">TOTAL TAXES</th>
				<th></th>
			</tr>	
			<tr>
				<th style="background: #A9A9A9;">TOTAL TOUTES TAXES</th>
				<th></th>
			</tr>
		</tbody>
	</table>
	
		<div style="display:inline-block; margin-top: 30px;">
			<div style=" float: left;margin: 10px;">
			DRESSER PAR<br>
			Telec Engineering<br>
			Tunis, le .......................
		    </div>
		    <div style=" float: right;margin: 10px;">
			LU ET ACCEPTER PAR<br>
			L'Entrepreneur<br>
			Tunis, le .......................
		    </div>
		</div>
		<div style="margin-top: 90px; text-align: center;" >
			<p>VU ET APPROUVER PAR<br>
			LE MAITRE DE L'OUVRAGE<br>
			Tunis, le .......................</p>
		</div>
		
	
</div>