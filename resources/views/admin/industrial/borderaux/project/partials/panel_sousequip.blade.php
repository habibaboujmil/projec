
<!-- afficher l'equipement avec les sousequip correspondant-->
@if($equi->has_sousequip == 1)
<table width="100%">
    <thead >
        <tr>
            <td width="15px"></td>
        	<th width="30px">
            <input type="hidden"  name="equipment[{{$equi->id}}]" value="" >
            <input type="checkbox" name="equipment[{{$equi->id}}]" data-toggle="collapse" data-target="#ch{{$equi->id}}"  onclick="change('c{{$equi->id}}')"  value="{{$equi->id}}" id="equip{{$equi->id}}"
            @if (isset ($equip_data)){{ in_array($equi->id,  $equip_data) ? 'checked' : '' }} @endif
            {{ in_array($equi->id, old('equipment', [])) ? 'checked' : '' }}>
            </th>




            <th width="20px"><i id="c{{$equi->id}}"class="fa fa-chevron-right" aria hidden="true"></i></th>
            <th data-toggle="collapse" data-target="#ch{{$equi->id}}" onclick="change('c{{$equi->id}}')" >
            {{$equi->app_display}}
            </th>
            <th width="120px"></th>
            <th width="20px"></th>
            <th width="20px"></th>
        </tr>
    </thead>
    <tbody id="ch{{$equi->id}}" class="collapse" style="margin-top: 20px">
    <p style="display: none;">{{$sousequip=App\models\sousequipement::sous_equip( $equi->id)}}</p>
        	@foreach($sousequip as $key=> $sousequi)
            @include('admin.industrial.borderaux.project.partials.panel_category_sousequip_list', ['sous_equip_collapse' =>$equi->id])
            @endforeach
    </tbody>
</table>
@endif


@include('admin.industrial.borderaux.project.partials.panel_category_equip_list')




