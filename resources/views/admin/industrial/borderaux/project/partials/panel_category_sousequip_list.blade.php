


@if(!empty($sousequi->category_id))
<tr>
    <td width="15px"></td>
    <td width="30px"></td>
    <td width="20px"></td>
    <td data-toggle="collapse" data-target=".scatg{{$sousequi->category_id}}" onclick="change('scateg{{$sousequi->category_id}}')" >
        <i id="scateg{{$sousequi->category_id}}"class="fa fa-chevron-right" aria hidden="true"></i>
       {{$category[($sousequi->category_id)-1]->category_title}}
    </td>
    <td></td>
</tr>
        
         <p style="display: none;">{{$sequi=App\models\sousequipement::sousequip_of_category($sousequi->category_id)}}</p>
         @foreach($sequi as $sequi)
<tr class="scatg{{$sousequi->category_id}} collapse" id="scatg{{$sousequi->category_id}}" style="margin-top: 20px"> 
<td width="15px"></td>
<td></td>
<td></td>

<td>  
<table> 
    <tbody >
        <tr >
            <td width="15px"><i id="cros{{$sequi->id}}" ></i></td>
            <td width="10px">
                <input type="hidden"  name="sousequipment[{{$sequi->id}}]" value="" >
                <input onclick="changeborder('sousquan{{$sequi->id}}')"type="checkbox" name="sousequipment[{{$sousequi->id}}]" value="{{$sequi->id}}"  
                @if (isset ($sousequip_data))  {{ in_array($sequi->id,  $sousequip_data) ? 'checked' : '' }}@endif  
                {{ in_array($sequi->id, old('sousequipment', [])) ? 'checked' : '' }} >

    <td>

    </td>
            <td width="20px">
                @if($sequi->duplicate ==1 )
                <i class="fa fa-plus" onclick="duplicate('b{{$sequi->id}}')"></i>
                @endif
                </td>
            <td data-toggle="collapse" data-target="#catg{{$equi->category_id}}" onclick="change('categ{{$equi->category_id}}')" >
               {{$sequi->app_display}}
                @if($sousequi->descrip ==1 )
                <input style="border:none; " autocomplete="off" placeholder="...................."type="text" name="souspcial[{{$sequi->id}}][]"
                @if(isset($errors))value=" {{old('souspcial')[$sousequi->id][0]}} "
                @elseif(isset($souspcial_data)) value=" {{empty($souspcial_data[$sequi->id]) ?  : implode($souspcial_data[$sequi->id])}}"@endif >
        @endif
            </td>
          
        </tbody>
        </table> 
    </td>
    <td style="padding-top: 3px" width="120px">
        <input id="sousquan{{$sequi->id}}" autocomplete="off" style=" width:40px; border-color: gainsboro"  placeholder=".........." type="text" name="sousquantite[{{$sequi->id}}][]" 
        @if(isset($sousquan_data) and $errors->isEmpty()) value=" {{empty($sousquan_data[$sequi->id]) ?: implode($sousquan_data[$sequi->id])}}"
        @elseif(isset($errors) )value=" {{old('sousquantite')[$sequi->id][0]}} "
        @endif> {{$sequi->sousequi_unit_abre}} 
                    
    </td>
    <td>
        <input type="checkbox" name="sousfour[{{$sequi->id}}][]"
        {{ isset($sousfour_data[$sequi->id]) ? 'checked' : '' }}
        {{ in_array("on", old('sousfour.'.$sequi->id, [])) ? 'checked' : '' }}>
    </td>
    <td>
        <input type="checkbox" name="souspose[{{$sequi->id}}][]" 
        {{ isset($souspose_data[$sequi->id]) ? 'checked' : '' }}
        {{ in_array("on", old('souspose.'.$sequi->id, [])) ? 'checked' : '' }}>
    </td>
    <td width="20px">
        <input type="checkbox" name="souspm[{{$sequi->id}}][]" 
        {{ isset($souspm_data[$sequi->id]) ? 'checked' : '' }}
        {{ in_array("on", old('souspm.'.$sequi->id, [])) ? 'checked' : '' }}>
    </td>
        
    
</tr>

@if($errors->has('sousquantite.'.$sequi->id.'.0'))
    
    <script type="text/javascript">
        $(document).ready(function() {
         errors_quantite ('sousquan{{$sequi->id}}','{{$collapse_id}}','cros{{$sequi->id}}','','{{$sous_equip_collapse}}'
            );
         });
    </script>
@endif
@if($errors->has('sousequipment.'.$sequi->id))      
    <script type="text/javascript">
        $(document).ready(function() {
         errors_equip ('sousquan{{$sequi->id}}','{{$collapse_id}}','cros{{$sequi->id}}','','{{$sous_equip_collapse}}');
         });



    </script>   
@endif 

@endforeach  




<!-- afficher liste des sousequip au cas il n'y a pas de category-->
       
@elseif(empty($sousequi->category_id))

<tr id="b{{$sousequi->id}}">
    <td width="15px"><i id="cros{{$sousequi->id}}" ></i></td>
    <td>
        <input type="hidden"  name="sousequipment[{{$sousequi->id}}]" value="" >
        <input type="checkbox" name="sousequipment[{{$sousequi->id}}]" id="sousequip{{$sousequi->id}}" value="{{$sousequi->id}}" onclick="changeborder('sousquan{{$sousequi->id}}')"
         @if (isset ($sousequip_data))  {{ in_array($sousequi->id,  $sousequip_data) ? 'checked' : '' }}@endif 
         {{ in_array($sousequi->id, old('sousequipment', [])) ? 'checked' : '' }} >

    </td>

    <td >
        @if($sousequi->duplicate ==1 )
        <i class="fa fa-plus" onclick="duplicate('b{{$sousequi->id}}')"></i>
        @endif
    </td>
    <td>
        {{$sousequi->app_display}} @if(!empty($sousequi->calibre)) ( {{$sousequi->calibre}} ) @endif
        @if($sousequi->descrip ==1 )
        <input style="border:none; " autocomplete="off" placeholder="...................."type="text" name="souspcial[{{$sousequi->id}}][]"
        @if(isset($errors))value=" {{old('souspcial')[$sousequi->id][0]}} "
        @elseif(isset($souspcial_data)) value=" {{empty($souspcial_data[$sousequi->id]) ?  : implode($souspcial_data[$sousequi->id])}}"@endif >
        @endif


    </td>
    <td style="padding-top: 3px">
        <input id="sousquan{{$sousequi->id}}" autocomplete="off" style=" width:40px; border-color: gainsboro"  placeholder=".........." type="text" name="sousquantite[{{$sousequi->id}}][]" 
        @if(isset($sousquan_data)and $errors->isEmpty()) value=" {{empty($sousquan_data[$sousequi->id]) ?: implode($sousquan_data[$sousequi->id])}}"
        @elseif(isset($errors))value=" {{old('sousquantite')[$sousequi->id][0]}} "
        @endif> {{$sousequi->sousequi_unit_abre}}
        
      

    </td>
    <td>
        <input type="checkbox" name="sousfour[{{$sousequi->id}}][]"
        {{ isset($sousfour_data[$sousequi->id]) ? 'checked' : '' }}
        {{ in_array("on", old('sousfour.'.$sousequi->id, [])) ? 'checked' : '' }}>
    </td>
    <td>
        <input type="checkbox" name="souspose[{{$sousequi->id}}][]" 
        {{ isset($souspose_data[$sousequi->id]) ? 'checked' : '' }}
        {{ in_array("on", old('souspose.'.$sousequi->id, [])) ? 'checked' : '' }}>
    </td>
    <td width="20px">
        <input type="checkbox" name="souspm[{{$sousequi->id}}][]"
        {{ isset($souspm_data[$sousequi->id]) ? 'checked' : '' }}
        {{ in_array("on", old('souspm.'.$sousequi->id, [])) ? 'checked' : '' }}>
    </td>
</tr>

@if($errors->has('sousquantite.'.$sousequi->id.'.0'))
    
    <script type="text/javascript">
        $(document).ready(function() {
         errors_quantite ('sousquan{{$sousequi->id}}','{{$collapse_id}}','cros{{$sousequi->id}}','','{{$sous_equip_collapse}}'
            );
         });
    </script>
@endif
@if($errors->has('sousequipment.'.$sousequi->id))      
    <script type="text/javascript">
        $(document).ready(function() {
         errors_equip ('sousquan{{$sousequi->id}}','{{$collapse_id}}','cros{{$sousequi->id}}','','{{$sous_equip_collapse}}');
         });

    </script>   
@endif 


@endif