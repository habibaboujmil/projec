@if(!empty($equi->category_id))

<p style="display: none;">{{$equip=App\equipment::equip_of_category($equi->category_id)}}</p>
<table width="100%">
    <thead>
        <tr>
            <td width="15px"></td>
            <td width="30px"></td>
            <td width="20px"><i id="categ{{$equi->category_id}}"class="fa fa-chevron-right" aria hidden="true"></i></td>
            <th data-toggle="collapse" data-target="#catg{{$equi->category_id}}" onclick="change('categ{{$equi->category_id}}')" >
               {{$category[($equi->category_id)-1]->category_title}}
            </th>
            <td></td>
        </tr>
    </thead>
    <tbody id="catg{{$equi->category_id}}" class="collapse" style="margin-top: 20px">
        @foreach($equip as $equip)
        <tr id="tr{{$equip->id}}">

            <td width="15px"><i id="cross{{$equip->id}}" ></i></td>                     
            <td width="30px">

                <input type="hidden"  name="equipment[{{$equip->id}}]" value="" >
                <input type="checkbox" name="equipment[{{$equip->id}}]"id="equip{{$equip->id}}"  value="{{$equip->id}}"
                onclick="changeborder('quantite{{$equip->id}}')"
                @if (isset ($equip_data[$equip->id])) {{ in_array($equip->id,  $equip_data) ? 'checked' : '' }}@endif 
                {{ in_array($equip->id, old('equipment', [])) ? 'checked' : '' }}>
            </td>
            <td width="20px">
                @if($equip->duplicate ==1 )
                <i class="fa fa-plus" onclick="duplicate('tr{{$equip->id}}')"></i>
                @endif
            </td>
            <td >
                {{$equip->app_display}}
                @if($equip->descrip ==1 )
                <input style="border:none; " autocomplete="off" placeholder="...................." type="text" name="spcial[{{$equip->id}}][]" 
                @if(isset($errors) and $errors->isEmpty())value=" {{old('spcial')[$equip->id][0]}} "
                @elseif(isset($spacial_data))value="{{empty($spcial_data[$equip->id])?: implode($spcial_data[$equip->id])}}"@endif
                > 
                @endif
                 
            </td>
            
            <td width="120px" style="padding-top: 3px" >
                <input id="quantite{{$equip->id}}" autocomplete="off" style=" width:40px; border-color: gainsboro;" placeholder=".........." type="text" name="quantite[{{$equip->id}}][]"
                 @if(isset($quantite_data) and $errors->isEmpty()) value=" {{empty($quantite_data[$equip->id]) ?  : implode($quantite_data[$equi->id])}}"
                 @elseif(isset($errors))value=" {{old('quantite')[$equip->id][0]}} " @endif>
                <!--<input type="hidden"  value="{{$i++}}"-->  
            
                {{$equi->equipment_unit_abre}}
                 
            </td>

            <td width="20px">
                <input type="checkbox" name="four[{{$equip->id}}][]" {{ isset($four_data[$equip->id]) ? 'checked' : '' }}
                {{ in_array("on", old('four.'.$equip->id, [])) ? 'checked' : '' }}>
            </td>
            <td width="20px">
                <input type="checkbox" name="pose[{{$equip->id}}][]" 
                {{ isset($pose_data[$equip->id]) ? 'checked' : '' }} 
                {{ in_array("on", old('pose.'.$equip->id, [])) ? 'checked' : '' }}>
            </td>
            <td width="20px">
                <input type="checkbox" name="pm[{{$equip->id}}][]" {{ isset($pm_data[$equip->id]) ? 'checked' : '' }}
                {{ in_array("on", old('pm.'.$equip->id, [])) ? 'checked' : '' }}>
            </td>
        </tr>

@if($errors->has('quantite.'.$equip->id.'.0'))
    
    <script type="text/javascript">
        $(document).ready(function() {
         errors_quantite ('quantite{{$equip->id}}','{{$collapse_id}}','cross{{$equip->id}}','catg{{$equi->category_id}}',''
            );
         });
    </script>
@endif
@if($errors->has('equipment.'.$equip->id))      
    <script type="text/javascript">
        $(document).ready(function() {
         errors_equip ('quantite{{$equip->id}}','{{$collapse_id}}','cross{{$equip->id}}','catg{{$equi->category_id}}','');
         });
    </script>   
@endif
        @endforeach
    </tbody>
</table>

 



@elseif(empty($equi->category_id)and empty($equi->has_sousequip))
<tr id="tr{{$equi->id}}" >
    <td width="15px"><i id="cross{{$equi->id}}" ></i></td>                       
    <td width="30px" >
        
        <input type="hidden"  name="equipment[{{$equi->id}}]" value="" >
        <input type="checkbox" name="equipment[{{$equi->id}}]"id="equip{{$equi->id}}" value="{{$equi->id}}" onclick="changeborder('quantite{{$equi->id}}')"
        @if (isset ($equip_data[$equi->id])) {{ in_array($equi->id,  $equip_data) ? 'checked' : '' }}@endif 
        {{ in_array($equi->id, old('equipment', [])) ? 'checked' : '' }}>
    
    </td>

   
    <td width="20px">
        @if($equi->duplicate ==1 )
        <i class="fa fa-plus" onclick="duplicate('tr{{$equi->id}}')"></i>
        @endif
    </td>
    <td>
        <strong>{{$equi->app_display}} @if(!empty($equi->calibre)) ( {{$equi->calibre}} ) @endif </strong>
        @if($equi->descrip ==1 )
        <input style="border-color: gainsboro;width:90px; " autocomplete="off" placeholder="...................." type="text" name="spcial[{{$equi->id}}][]" 
        @if(isset($spcial_data) and $errors->isEmpty()) value=" {{empty($spcial_data[$equi->id]) ?: implode($spcial_data[$equi->id])}}"
        @elseif(isset($errors))value=" {{old('spcial')[$equi->id][0]}} "@endif > 
        @endif
         
    </td>
    <td width="120px" style="padding-top: 3px" >
        <input id="quantite{{$equi->id}}"  style=" width:40px; 
         border-color: gainsboro;" placeholder=".........." type="text" name="quantite[{{$equi->id}}][]"
         
          
          @if(isset($quantite_data) and $errors->isEmpty()) value=" {{empty($quantite_data[$equi->id]) ?  : implode($quantite_data[$equi->id])}}"
          @elseif(isset($errors)) value=" {{old('quantite')[$equi->id][0]}}"     
          @endif
         >
        <!--<input type="hidden"  value="{{$i++}}"-->  
    
        {{$equi->equipment_unit_abre}}
         
    </td>
    <td width="20px">
        <input type="checkbox" name="four[{{$equi->id}}][]" {{ isset($four_data[$equi->id]) ? 'checked' : '' }}
        {{ in_array("on", old('four.'.$equi->id, [])) ? 'checked' : '' }}>
    </td>
    <td width="20px">
        <input type="checkbox" name="pose[{{$equi->id}}][]" 
        {{ isset($pose_data[$equi->id]) ? 'checked' : '' }} 
        {{ in_array("on", old('pose.'.$equi->id, [])) ? 'checked' : '' }}>
    </td>
    <td width="20px">
        <input type="checkbox" name="pm[{{$equi->id}}][]" {{ isset($pm_data[$equi->id]) ? 'checked' : '' }}
        {{ in_array("on", old('pm.'.$equi->id, [])) ? 'checked' : '' }}>
    </td>
                            
</tr>


@if($errors->has('quantite.'.$equi->id.'.0'))
    
    <script type="text/javascript">
        $(document).ready(function() {
         errors_quantite ('quantite{{$equi->id}}','{{$collapse_id}}','cross{{$equi->id}}','',''
            );
         });
    </script>
@endif
@if($errors->has('equipment.'.$equi->id))      
    <script type="text/javascript">
        $(document).ready(function() {
         errors_equip ('quantite{{$equi->id}}','{{$collapse_id}}','cross{{$equi->id}}','','');
         });
    </script>   
@endif                                

                            
@endif