<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <h5><br>Nouveau Projet</h5>
      </div>
      <div class="modal-body">
       <form action="{{url('admin/industrial/projects')}}" method="post">
        {{csrf_field()}}
          <div class="form-group ">
          <input type="HIDDEN" name="building_id" class="form-control" value="{{ $building->id }}"  >
          </div>


          <div class="form-group ">
          <label for="add_name">Nom de Projet </label>
          <input type="text" name="add_name" class="form-control" value="{{ old('add_name') }}">
          @if($errors->has('add_name'))      
            <i class="fa fa-times-circle" style="color: red;" >
              Ce champ est obligatoire! 
            </i>
                   
                @endif


        </div>
          <div class="form-group">
          <label for="add_reference">Référence</label>
          <input type="text" name="add_reference" class="form-control"
          @if ($errors-> has('add_reference')) value="{{ old('add_reference') }}"
          @else value="{{now()->format('Ymd')}}_" @endif >
          @if ($errors-> has('add_reference'))
          <i class="fa fa-times-circle" style="color: red;" >
            Ce champ est déjà pris!  
          </i>
          @endif
        </div>
         <div class="form-group">
          <label for="ouvrage">Maitre d'Ouvrage</label>
          <input type="text" name="ouvrage" class="form-control" >
          @foreach($errors->get('ouvrage') as $msg)
          <li style="color: red"> {{$msg}}</li>
          @endforeach
        </div>
         
          <div >
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
        <input class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="submit" name="form-control" value="sauvgarder">
        </div>
      </form>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>