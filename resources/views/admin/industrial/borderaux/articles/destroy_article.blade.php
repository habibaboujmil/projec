<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><strong>Attention</strong> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <strong> Voulez-vous vraiment supprimer cet article! </strong> 
      </div>
      <div class="modal-footer">
        
      <form action="{{url('admin/industrial/articles/'.$article->id)}}" method="post">
        {{csrf_field()}}
        {{method_field('DELETE')}}
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
        <button type="submit" class="btn btn-warning">supprimer</button>
      </form>
        
      </div>
    </div>
  </div>
</div>