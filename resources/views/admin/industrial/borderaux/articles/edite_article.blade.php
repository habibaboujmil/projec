 <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" align="center"><b> Editer </b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/industrial/articles/edit')}}" method="post">
                    <input type="hidden" name="_method" value="PUT">
                {{csrf_field()}}
            <input type="hidden" name="article_id" id="id" class="form-control">
                <div class="form-group ">
                    <label for="article_title">Nom de l'article </label>
                    <input type="text" name="article_title" class="form-control"id="article_title">
                    @foreach($errors->get('title') as $msg)
                    <li style="color: red"> {{$msg}}</li>
                    @endforeach
                </div>
                <div class="form-group">
                    <label for="article_description">Description</label>
                    <textarea  name="article_description" class="form-control" id="article_description" rows="9" cols="33" ></textarea>
                    @foreach($errors->get('article_description') as $msg)
                    <li style="color: red"> {{$msg}}</li>
                    @endforeach
                </div>
                <div>
                    <input type="checkbox" name="level" value="1" id="level">  Cette article est à 2 niveaux
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
                    <input class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="submit" name="form-control" value="sauvgarder">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    
        <script type="text/javascript">
         $('#edit').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var article_title = button.data('title') 
      var article_description = button.data('description') 
      var id = button.data('id') 
      var level = button.data('level') 

      var modal = $(this)
      modal.find('.modal-body #article_title').val(article_title);
      modal.find('.modal-body #article_description').val(article_description);
      modal.find('.modal-body #id').val(id);
      if (level == 1) {document.getElementById("level").checked = true;}
      
    })
    </script>

@endsection
