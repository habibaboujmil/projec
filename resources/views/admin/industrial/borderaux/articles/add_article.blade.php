@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
               
            </div>
          <div class="box-body">
       <form action="{{url('admin/industrial/articles')}}" method="post">
        {{csrf_field()}}
        <div class="form-group ">
          <input type="HIDDEN" name="lot_id" class="form-control" value="{{ $lot->id }}"  >
          <input type="HIDDEN" name="building_type" class="form-control" value="{{ $type}}"  >
        </div>
        <div class="form-group ">
          <label for="article_title">Nom de l'article </label>
          <input type="text" name="article_title" class="form-control" value="{{old('article_title')}}" required>
        </div>
          <div class="form-group">
          <label for="article_description">Description</label>
          <textarea name="article_description" class="form-control" value="{{old('article_description')}}" rows="9" cols="33"  >
          @foreach($errors->get('article_description') as $msg)
          <li style="color: red"> {{$msg}}</li>
          @endforeach
        </textarea>
        </div>
        <div class="form-group">
          <label for="equipment">equipement</label>
          <input type="text" name="equipment"  class="form-control" value="{{old('equipment')}}">
        </div>
        <div>
          <input type="checkbox" name="level" value="1">  Cette article est à 2 niveaux
        </div>
         <input class="btn btn-primary sub_btn"type="submit" name="article" value="suivant ">
        
      </form>
      
        
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .sub_btn{float: right;}
</style>
@endsection

