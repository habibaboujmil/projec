@extends('layouts.admin')

@section('content')

        @if(session()->has('success'))

        <div class="alert alert-success">
          <p>{{session()->get('success')}}</p>
        </div>
        @endif

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <span><i class="fa fa-table"></i></span>
                    <span>Liste des Article</span>
                </h3>
            </div>

            <div class="box-body">

                <div class="well well-sm well-toolbar">
                    <a href="{{url('admin/industrial/'.$type.'-'.$lot->id.'/articles')}}"class="btn btn-primary" type="button" ><i class="fa fa-fw fa-plus"></i>Ajouter un Article</a>
                        
                     
                     
                </div>
                
                <div>
                <table id="tbl-list" data-server="false" class="dt-table table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="20px">N°</th>
                        <th>Titre de l'article</th>
                        <th>Action </th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($article as $article)
                         <tr>
                            <td>{{$loop->index+1 }}</td>
                            <td>{{$article->article_title}}</td>
                            <td width="130px">
                                <a href="{{url('admin/industrial/equipments/'.$article->id)}}" class="btn btn-primary" > <i class="fa fa-table" data-toggle="tooltip" title="Détail"></i></a>
                                <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#edit" data-id="{{$article->id}}" data-title="{{$article->article_title}}" data-description="{{$article->article_description}}" data-level="{{$article->level}}"  >
                                    <i class="fa fa-pencil-square-o" data-toggle="tooltip" title="Info / Editer"></i>
                                </button>
                                 
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete"><i class="fa fa-trash-o"data-toggle="tooltip" title="Supprimer"></i></button>
                                @include('admin.industrial.borderaux.articles.destroy_article')
                             </td>
                         </tr>
                         @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            @include('admin.industrial.borderaux.articles.edite_article')
        </div>
    </div>
</div>
    <script type="text/javascript">
     $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
}); 
</script>
    @endsection
    

