<div>
<h3 style="text-align: center">SPECIFICATIONS GENERALES RELATIVES AUX PRIX UNITAIRES</h3>
<p>Tous les travaux décrits ci après devront être conformes aux normes et règlements en vigueur.</p>
<p>Les prix ci-dessous désignés s’étendent aux ouvrages et travaux réellement exécutés conformément aux plans ou résultant des dispositions agréées par le Maître d’Ouvrage. Le forfait des prix unitaires comprend d’une manière générale :</p>
<p>1-      Le traçage, l’implantation des ouvrages, et la pose des ensembles électriques.<br>
2-      La fourniture, l’amenée, le façonnage, le repli éventuel de tous les matériels et matériaux nécessaires aux travaux.<br>
3-      Les travaux, installations provisoires, magasins, bureaux dépendances etc.… nécessaires au stockage de matériels et au déroulement des travaux.<br>
4-      Les démolitions et percements nécessaires aux travaux.<br>
5-      Les gaines et fourreautages des éléments encastrés.<br>
6-       La fourniture et pose de raccords, joints d’étanchéité, boulons, coudes, chevilles, colliers autant que nécessaires, et tous les accessoires nécessaires à la bonne exécution des travaux raccordement des lignes aux compteurs.<br>
7-      La présentation d’un échantillon avant toutes poses et montages.<br>
8-      Les rebouchages et remise en état des démolitions.<br>
9-      Le maître de l’ouvrage a la latitude, s’il le désire de changer les types des équipements. Les augmentations ou diminutions résultant de son choix seront décomptés à l’entrepreneur en plus ou en moins.<br>
10-  Le maître de l’ouvrage a la latitude, s’il le désire de fournir à l’entreprise sur chantier, tous ou une partie des équipements décrits dans le bordereau des prix. Dans ce cas l’entrepreneur devra donner une décharge pour les équipements fournis et en deviendra entièrement responsable. L’entrepreneur aura à sa charge la pose de ces équipements qui comprend d’une manière générale :
	<ul>
		<li>Le déchargement des équipements fournis par le maître de l’ouvrage rendu sur le chantier et leur stockage dans un magasin (fournis par l’entrepreneur).</li>
		<li>La protection de tous les équipements contre les intempéries et le vol jusqu’à leur utilisation.</li>
		<li> Les manutentions et les accessoires de pose, de raccordement et de fixation.</li>
		<li>Le nettoyage et la protection des ouvrages jusqu’à la réception provisoire.</li>
	</ul>
</br>
11-  L’entrepreneur est le seul responsable jusqu’à la réception provisoire en cours de chantier, des équipements détériorés, dégradés ou volés. Il est tenu de les remplacer à ses frais et sans aucune indemnité.<br>
12-  La fourniture du matériel et matériaux (fluides compris) nécessaires aux essais obligatoires en vue de la réception des installations.<br>
13-  Les travaux nécessaires aux raccordements électriques.<br>
14-  Les frais de compte prorata.<br>
15-  Les garanties des matériels installés.</p>
</div>
<div style="float: right;">
	Lu et accepté par l'entreprise soumissionaire <br>Tunsi le: … … … … … …</p>
</div>
