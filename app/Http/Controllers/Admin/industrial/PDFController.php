<?php

namespace App\Http\Controllers\admin\industrial;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\models\building_type;
use App\models\projectliste;
use App\Models\projectlot;
use App\Models\lot;
use App\Models\sousequipement;
use App\Models\specification;
use App\models\commun_clause;
use App\article_bord;
use App\equipment;
use Illuminate\Support\Facades\Auth;
use PDF;

class PDFController extends AdminController
{
    public function save(Request $Request)
    { 
      
        
        if ($Request->submit =='reset')
        {

            $input_to_save=NULL;
            $lot=projectlot::where('lot_id',$Request->lot)->where('project_id',$Request->project_id)->first();
            $lot->lotcontent=$input_to_save;
            $lot->save();
        }
        else
        {
             $Request->validate([
        'article' => 'required|min:1',
        'quantite.*.*' => 'numeric|nullable|required_with:equipment.*',
        'equipment.*' => 'required_with:quantite.*.0',
        'sousquantite.*.*' => 'numeric|nullable|required_with:sousequipment.*',
        'sousequipment.*' => 'required_with:sousquantite.*.0',
       
                
         ]);



            $input_to_save=$Request->all();
            $lot=projectlot::where('lot_id',$Request->lot)->where('project_id',$Request->project_id)->first();
            $lot->lotcontent=json_encode($input_to_save);
            $lot->updated_by=Auth::user()->firstname;
            $lot->save();
        }
       
        
        return back();
    }
            
    public function PDFborderaux($project_id,$lot_id)
    {
        
        $lot=projectlot::where('lot_id',$lot_id)->where('project_id',$project_id)->first();
        $lot_info=lot::where('id',$lot_id)->first();
        $specification=specification::find(1);
        
        $lot_name=$lot->lot;
        $input=$lot->lotcontent; 
        $data= json_decode($input);

        $article = $data->article;
        $equip = (array) $data->equipment;
        $equipment = array_filter( $equip);
        
        $reference=$data->reference;
        $project=$data->project;
      
        if (isset($data->sousquantite)) {$sousquan= (array) $data->sousquantite;}
        else{$sousquan=NULL;}
        if (isset($data->quantite)) {$quantite= (array) $data->quantite;}
        else{$sousquan=NULL;}
        if (isset($data->four)) {$four= (array) $data->four;}
        else{$four=NULL;}
        if (isset($data->pose)){$pose=(array)$data->pose;}
        else{$pose=NULL;} 
        if (isset($data->sousfour)){$sousfour=(array) $data->sousfour;}
        else{$sousfour=NULL;}
        if (isset($data->souspose)){$souspose=(array) $data->souspose;}
        else{$souspose=NULL;}
        if (isset($data->spcial)){$spcial=(array) $data->spcial;}
        else{$spcial=NULL;}


        if (isset($data->souspcial)){$souspcial=(array) $data->souspcial;}
        else{$souspcial=NULL;}
        if (isset($data->pm)){$pm=(array) $data->pm;}
        else{$pm=NULL;}
        if (isset($data->souspm)){$souspm=(array) $data->souspm;}
        else{$souspm=NULL;}
        $total='A';
        foreach ($article as $id ) 
        {
            $articles[]=article_bord::find($id);
            $total_article[]=$total;
            $total++;
        }
            $break=0;
            $tot_price=0;
        foreach ($equipment as $ids) 
        {
            $equipments[]=equipment::find($ids);
            $break++;
        }
        if (isset($data->sousequipment))
         {
            $sousbreak=0;
            foreach($data->sousequipment as $key) 
                {
                    $sousequi[]=sousequipement::find($key);
                    $sousbreak++;
                }
            $sousequip = array_filter( $sousequi);

         } 
        else {$sousequip[]=NULL;}
       
         
         
     
        
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf = PDF::loadView('admin.industrial.borderaux.project.projectcontent.pdf_bordereaux',compact('sousquan','sousequip','equipments','articles','break','quantite','total_article','project','lot_name','reference','pose','four','souspose','sousfour','spcial','souspcial','pm','souspm','specification','lot_info'));
        $file_name=$reference.'_BORD_'.$lot_info->lot_name;    
        return $pdf->stream($file_name.'.pdf',array('Attachment'=>'telec'));

    }


     public function PDFestimation($project_id,$lot_id)
    {
         
        $lot=projectlot::where('lot_id',$lot_id)->where('project_id',$project_id)->first();
        $lot_name=$lot->lot;
        $lot_info=lot::where('id',$lot_id)->first();
        $specification=specification::find(1);
        $input=$lot->lotcontent; 
        $data= json_decode($input);

        $article = $data->article;
        $equipmen = (array) $data->equipment;
        $equipment = array_filter($equipmen);
        
        $reference=$data->reference;
        $project=$data->project;
          if (isset($data->sousequipment))
         {
            $sousbreak=0;
            foreach ($data->sousequipment as $key) 
                {
                    $sousequi[]=sousequipement::find($key);
                    $sousbreak++;
                    $sousequip = array_filter($sousequi);
                }

         } else {$sousequip[]=NULL;}


        if (isset($data->sousquantite)) {$sousquan= (array) $data->sousquantite;}
        else{$sousquan=NULL;}
        if (isset($data->quantite)) {$quantite= (array) $data->quantite;}
        else{$sousquan=NULL;}
        if (isset($data->four)) {$four= (array) $data->four;}
        else{$four=NULL;}
        if (isset($data->pose)){$pose=(array)$data->pose;}
        else{$pose=NULL;} 
        if (isset($data->sousfour)){$sousfour=(array) $data->sousfour;}
        else{$sousfour=NULL;}
        if (isset($data->souspose)){$souspose=(array) $data->souspose;}
        else{$souspose=NULL;}
        if (isset($data->spcial)){$spcial=(array) $data->spcial;}
        else{$spcial=NULL;}
        if (isset($data->pm)){$pm=(array) $data->pm;}
        else{$pm=NULL;}
        if (isset($data->souspm)){$souspm=(array) $data->souspm;}
        else{$souspm=NULL;}

        if (isset($data->souspcial)){$souspcial=(array) $data->souspcial;}
        else{$souspcial=NULL;}
        $total='A';
        foreach ($article as $id ) 
        {
            $articles[]=article_bord::find($id);
            $total_article[]=$total;
            $total++;
        }
            $break=0;
            $tot_price=0;
        foreach ($equipment as $ids) 
        {
            $equipments[]=equipment::find($ids);
            $break++;
        }
         $sousbreak=0;
       

        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('isRemoteEnabled', TRUE);
        $pdf->getDomPDF()->set_option("enable_php", true);

        $pdf = PDF::loadView('admin.industrial.borderaux.project.projectcontent.pdf_estimation',compact('sousquan','sousequip','equipments','articles','break','quantite','total_article','project','lot_name','reference','pose','four','souspose','sousfour','spcial','souspcial','pm','souspm','lot_info','specification'));
        $file_name=$reference.'_BORD_'.$lot_info->lot_name;
        return $pdf->stream($file_name.'.pdf');
    }





    public function PDFcptp($project_id,$lot_id)
    {

       $lot_info=lot::where('id',$lot_id)->first();
       
       $project=projectliste::where('id',$project_id)->first();

       
       
       $last_clauses=commun_clause::where('position',1)->where('parent_id',0)->where('lot_id',$lot_id)->where('building_id',$project->building_id)->get();
       $content=projectlot::where('lot_id',$lot_id)->where('project_id',$project_id)->first();


        $data= json_decode($content->lotcontent);
        foreach ($data->article as $key=> $id ) 
        {

            $articles[]=article_bord::where('id',$id)->first();
            $article_content[]= json_decode($articles[$key]->article_clause);
            
        }

       
        
        
        if (isset($data->equipment)) {
            $equip = (array) $data->equipment;
            $equipment = array_filter( $equip);
        }
        
        $index=0;
        
           
         if (!empty($equipment)) 
         {
            foreach ($equipment as $ids ) 
            {
            
                $equipments[]=equipment::where('id',$ids)->first();
                $equipment_content[]= json_decode($equipments[$index]->equipment_clause);
                $index++;  
            }
        }
        else {$equipments[]=NULL; $equipment_content[]=NULL; }

        
        $break = count($equipments);

        $index1=0;
        
            if (isset($data->sousequipment)) {
                
                $sousequip = (array) $data->sousequipment;
                $sousequipment = array_filter( $sousequip);
                
                if (!empty($sousequipment)) {
                    
                foreach ($sousequipment as $ids ) 
                {
                    if (isset($ids)) 
                    {   
                        $sousequipments[]=sousequipement::where('id',$ids)->first();
                        $sousequip_content[]= json_decode($sousequipments[$index1]->sousequi_clause);
                        $index1++;
                    }
                    else {$sousequipments[]=NULL; $sousequip_content[]=NULL; }
                }}
                else {$sousequipments[]=NULL; $sousequip_content[]=NULL; }
            }else {$sousequipments[]=NULL; $sousequip_content[]=NULL; }
            
        
        $sous_break = count($sousequipments);
        
        if ($lot_info->id == 1) 
        {  
            $article_content[1]->l1_file= (array) $article_content[1]->l1_file ;
           $cptp_input= json_decode($content->cptp_input)->input;

           $first_clauses=commun_clause::where('position',0)->where('parent_id',0)->where('lot_id',$lot_id)->where('building_id',$project->building_id)->get();
           $pdf =\App::make('dompdf.wrapper');
           $pdf->getDomPDF()->set_option("enable_php", true);
           $pdf = PDF::loadView('admin.industrial.CPTP.partials.cfo_cptp', compact('lot_info','first_clauses','last_clauses','project','articles','article_content','equipments','equipment_content','break','sousequipments','sousequip_content','sous_break','cptp_input'));
        $file_name=$project->reference.'_CPTP_'.$lot_info->lot_name;
       
         return $pdf->stream($file_name.'.pdf'); 
        }
        
        $first_clauses=commun_clause::where('position',0)->where('parent_id',0)->where('lot_id',$lot_id)->where('building_id',$project->building_id)->get();
        $pdf =\App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf = PDF::loadView('admin.industrial.CPTP.PDF_cptp', compact('lot_info','first_clauses','last_clauses','project','articles','article_content','equipments','equipment_content','break','sousequipments','sousequip_content','sous_break'));
        $file_name=$project->reference.'_CPTP_'.$lot_info->lot_name;
         return $pdf->stream($file_name.'.pdf');
    } 
}
