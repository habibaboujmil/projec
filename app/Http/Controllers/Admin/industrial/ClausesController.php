<?php

namespace App\Http\Controllers\admin\industrial;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdminController;
use App\models\commun_clause;
use App\models\lot;
use App\models\building_type;
Use Redirect;

class ClausesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_clause($building_id,$lot_id)
    {
        return $this->view('industrial.CPTP.partials.form_add_clause',compact('building_id','lot_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $new = new commun_clause();
        $new->lot_id=$request->lot_id;
        $new->building_id=$request->building_id;
        $new->parent_id=0;
        $new->title=$request->l1_title;
        $new->clause=$request->l1_clause;
        $new->position=$request->position;
        if ($request->hasFile('l1_file')) 
        { $new->clause_file = $request->file('l1_file')->store('clause_files','public');}
        
        $new->save();
        $last_id=commun_clause::latest('id')->pluck('id')->first();
        foreach ($request->l2_title as $key1 => $l2_title ) 
        {
            if (!empty($l2_title))
            { 

               $new = new commun_clause();
               $new->parent_id=$last_id;
               $new->title=$l2_title;
               $new->clause=$request->l2_clause[$key1];
               $new->save();
               $parent_id=commun_clause::latest('id')->pluck('id')->first();
               if (isset($l3_title)) {
                foreach ($request->l3_title[$key1] as $key2 => $l3_title) 
               {
                   if (!empty($l3_title))
                    {
                       $new = new commun_clause();
                       $new->parent_id=$parent_id;
                       $new->title=$l3_title;
                       $new->clause=$request->l3_clause[$key1][$key2];
                       $new->save();
                   }
            
                }
           }

            }
            
        }
        notify()->success('Success!', 'Une nouvelle clause à été enregistré avec succé.');
         return Redirect::to('/admin/industrial/clausescommune/'.$request->building_id.'/'.$request->lot_id);
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function show($type,$lot)
    {
        $lot_list =lot::find($lot);
        $building_type=$type;
        $building =building_type::all();
        $clause= commun_clause::where('lot_id', $lot)->where('building_id',$type)->where('parent_id',0)->orderBy('id')->get();
         
        return $this-> view('industrial.cptp.commun_clause',compact('building','clause','type'))->with(['lot'=>$lot_list]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
       
        foreach ($request->id as $key => $id) 
        {
            $clause = commun_clause::findOrFail($id);
            $clause->title=$request->title[$key];
            $clause->clause=$request->clause[$key];
            $clause->save();
        }
        notify()->success('Success!', 'le changement à été bien enregistré.');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
