<?php

namespace App\Http\Controllers\admin\industrial;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\article_bord;
use App\equipment;
use App\Models\categoryequip;
Use Redirect;

class EquipmentsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($articleid,$categid)
    {
       
       $article_id=$articleid;
       $categ_id=$categid;

        return $this->view('industrial.borderaux.equipments.add_equipment',compact('article_id','categ_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $Request)
    {
        
        if(isset($Request->equipment))
        {
            
        $equipment = new equipment();
        $equipment->app_display= $Request->app_display;
        $equipment->equipment_title= $Request->equipment_title;
        $equipment->equipment_description= $Request->equipment_description;
        $equipment->calibre= $Request->calibre;
        $equipment->equipment_unit= $Request->equipment_unit;
        $equipment->equipment_unit_abre= $Request->equipment_unit_abre;
        $equipment->four_price= $Request->four_price;
        $equipment->pose_price= $Request->pose_price;
        $equipment->duplicate= $Request->duplicate;
        $equipment->descrip= $Request->descrip;
        $equipment->article_id = $Request->article_id;
        $equipment->category_id = $Request->category_id;
        $equipment->save();
        $indication='equipment';

        return $this->view('industrial.borderaux.partials.add_clause',compact('indication',));} 
        
        if (isset($Request->clause)) {
         
        $data=$Request->all();
        
            if ($Request->hasFile('l1_file')) 
            {

            foreach ($Request->l1_file as $key => $file) 
                {
                    $file=$Request->l1_file[$key]->store('clause_files','public');
                    $data['l1_file'][$key]=$file;
                }   
                
            }
            
            if (isset($Request->l2_file )) 
            {
               
                foreach ($Request->l2_file as $key => $file) 
                {
                    foreach ($file as $key1 => $file_l2)
                    { 
                        $file_path=$file_l2->store('clause_files','public');
                        $data['l2_file'][$key][$key1]=$file_path;
                    } 
                }
                
            }
            
            if (isset($Request->l3_file )) 
            {
                foreach ($Request->l3_file as $key => $file) 
                {
                    foreach ($file as $key1 => $file_l2)
                    {
                        foreach ($file_l2 as $key2 => $file_l3) 
                        {
                            $file_path=$file_l3->store('clause_files','public');

                            $data['l3_file'][0][0][0]=$file_path;
                        }  
                    }  
                }
            }
        
        $equipment =equipment::latest('id')->first();
        $equipment->equipment_clause=json_encode($data);
        $equipment->save();
        notify()->success('Success!', "l'élement à été enregistré avec succé.");

        return Redirect::to('admin/industrial/equipments/'.$equipment->article_id);

        }

    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article =article_bord::find($id);
        $equipment= equipment::where('article_id', $id)->where('category_id', NULL)->get();
        $category=categoryequip::where('article_id', $id)->get();
       
        return $this->view('industrial.borderaux.equipments.equipment_list',compact('equipment','category','article'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request )
    {
        
        $equipment = equipment::findOrFail($Request->equipment_id);
        $equipment->app_display= $Request->input('app_display');
        $equipment->equipment_title= $Request->input('equipment_title');
        $equipment->equipment_description= $Request->input('equipment_description');
        $equipment->calibre= $Request->input('calibre');
        $equipment->equipment_unit= $Request->input('equipment_unit');
        $equipment->equipment_unit_abre= $Request->input('equipment_unit_abre');
        
        $equipment->four_price= $Request->input('four_price');
        $equipment->pose_price= $Request->input('pose_price');
        $equipment->duplicate= $Request->input('duplicate');
        $equipment->descrip= $Request->input('descrip');
        
        
        $equipment->save();
       
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        $equipment=equipment::find($id);
        $equipment->delete();
        return $this->view('industrial.borderaux.equipments.equipment_list');
    }

    public function equip_of_category($category_id,$arctile_id)
    {
        $category =categoryequip::find($category_id);
        $equipment= equipment::where('category_id', $category_id)->get();
        $article =article_bord::find($arctile_id);
        return $this->view('industrial.borderaux.equipments.category.category_equip_list',compact('equipment','category','article'));

    }
}
