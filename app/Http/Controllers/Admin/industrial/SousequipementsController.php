<?php

namespace App\Http\Controllers\admin\industrial;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\article_bord;
use App\equipment;
use App\Models\sousequipement;
use App\Models\categoryequip;
Use Redirect;

class SousequipementsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function create($equipid ,$categid)
    {
       $equip_id=$equipid;
       $category_id=$categid;
        return $this->view('industrial.borderaux.sousequip.add_sousequi',compact('equip_id','category_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $Request)
    {
        
        if(isset($Request->sous_equip))
        {
            $sousequi = new sousequipement();
            $sousequi->app_display= $Request->app_display;
            $sousequi->sousequi_title= $Request->sousequi_title;
            $sousequi->sousequi_description= $Request->sousequi_description;
            $sousequi->calibre= $Request->calibre;
            $sousequi->sousequi_unit= $Request->sousequi_unit;
            $sousequi->sousequi_unit_abre= $Request->sousequi_unit_abre;
            $sousequi->four_price= $Request->four_price;
            $sousequi->pose_price= $Request->pose_price;
            $sousequi->duplicate= $Request->duplicate;
            $sousequi->descrip= $Request->descrip;
            $sousequi->equi_id = $Request->equi_id;
            $sousequi->category_id = $Request->category_id;
            $sousequi->save();

            $equipment= equipment::find($Request->equi_id);
            if ($equipment->has_sousequip == NULL)
            {
                $equipment->has_sousequip = 1;
                $equipment->save();
            }
            $indication='sousequip';
            return $this->view('industrial.borderaux.partials.add_clause',compact('indication'));
        }

        if (isset($Request->clause)) {
         
        $data=$Request->all();
        $sousequi =sousequipement::latest('id')->first();
        $sousequi->sousequi_clause=json_encode($data);
        $sousequi->save();
        
        return Redirect::to('admin/industrial/sousequipments/'.$sousequi->equi_id);

        }
        
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article =equipment::find($id);
        $equipment= sousequipement::where('equi_id', $id)->where('category_id', NULL)->get();
        $category=categoryequip::where('equip_id', $id)->get();
        return $this->view('industrial.borderaux.sousequip.sousequip_list',compact('equipment','category','article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request)
    {
        return $Request;
        $sousequi = sousequipement::findOrFail($Request->equipment_id);
        $sousequi->sousequi_title= $Request->input('equipment_title');
        $sousequi->sousequi_description= $Request->input('equipment_description');
        $sousequi->app_display= $Request->input('app_display');
        $sousequi->sousequi_unit= $Request->input('equipment_unit');
        $sousequi->sousequi_unit_abre= $Request->input('equipment_unit_abre');
        
        $sousequi->four_price= $Request->input('four_price');
        $sousequi->pose_price= $Request->input('pose_price');
        $sousequi->duplicate= $Request->input('duplicate');
        $sousequi->descrip= $Request->input('descrip');
        
        $sousequi->save();
       
        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        $equipment=sousequipement::find($id);
        $equipment->delete();
        return $this->view('industrial.borderaux.sousequip.sousequip_list');
    }
    public function sousequip_of_category($category_id,$arctile_id)
    {
        $category =categoryequip::find($category_id);
        $equipment= sousequipement::where('category_id', $category_id)->get();
        $article =equipment::find($arctile_id);
        return $this->view('industrial.borderaux.sousequip.category.category_sousequip_list',compact('equipment','category','article'));
    }
}
