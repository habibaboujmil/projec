<?php

namespace App\Http\Controllers\admin\industrial;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdminController;
use App\article_bord;
use App\models\lot;
use App\models\building_type;
use Response;
Use Redirect;
use Validator;


class ArticlebordsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type,$lot)
    {
         $lot=lot::find($lot);
        $type=$type;
         return $this->view('industrial.borderaux.articles.add_article',compact('lot','type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = array('article_title' => 'required');

        if(isset($request->article))
        {   
            $article = new article_bord();
            $article->article_title= $request->article_title;
            $article->article_description= $request->article_description;
            $article->lot_id= $request->lot_id;
            if (isset($article->level))
                {$article->level= $request->level;}
            $article->building_type=$request->building_type;
            $article->equipment= $request->equipment;
            $article->save();
            $indication='article';
            return $this->view('industrial.borderaux.partials.add_clause',compact('indication'));
        }

        if (isset($request->clause)) {

        $data=$request->all();
        
            if ($request->hasFile('l1_file')) 
            {

            foreach ($request->l1_file as $key => $file) 
                {
                    $file=$request->l1_file[$key]->store('clause_files','public');
                    $data['l1_file'][$key]=$file;
                }   
                
            }
            
            if (isset($request->l2_file )) 
            {
               
                foreach ($request->l2_file as $key => $file) 
                {
                    foreach ($file as $key1 => $file_l2)
                    { 
                        $file_path=$file_l2->store('clause_files','public');
                        $data['l2_file'][$key][$key1]=$file_path;
                    } 
                }
                
            }
            
            if (isset($request->l3_file )) 
            {
                foreach ($request->l3_file as $key => $file) 
                {
                    foreach ($file as $key1 => $file_l2)
                    {
                        foreach ($file_l2 as $key2 => $file_l3) 
                        {
                            $file_path=$file_l3->store('clause_files','public');

                            $data['l3_file'][0][0][0]=$file_path;
                        }  
                    }  
                }
            }
        
        $article=article_bord::latest('id')->first();
        $article->article_clause=json_encode($data);
        $article->save();
        $lot=$article->lot_id;
        $building=$article->building_type;
        notify()->success('Success!', 'Un article à été enregistré avec succé.');

        return Redirect::to('/admin/industrial/articles/'.$building.'/'.$lot);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($type,$lot)
    {
        $lot_list =lot::find($lot);
        $building_type=$type;
        $building =building_type::all();
        $article= article_bord::where('lot_id', $lot)->where('building_type',$type)->orderBy('id')->get();
         
        return $this-> view('industrial.borderaux.articles.article_list',compact('building','article','type'))->with(['lot'=>$lot_list]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    
        $article = article_bord::findOrFail($request->article_id);
        $article->article_title= $request->input('article_title');
        $article->article_description= $request->input('article_description');
        $article->level= $request->input('level');
        $article->save();
       
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $article=article_bord::find($id);
        $article->delete();
        return back();
    }

}
