<?php

namespace App\Http\Controllers\admin\industrial;
use App\Http\Controllers\Admin\AdminController;
use Titan\Controllers\TitanAdminController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\building_type;
use App\models\projectliste;
use App\Models\projectlot;
use App\Models\lot;
use App\Models\sousequipement;
use App\Models\categoryequip;
use App\models\commun_clause;
use App\article_bord;
use App\Equipment;
use Response;
Use Redirect;
class projectsController extends AdminController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $Request)
    {
         $Request->validate([
        'add_reference' => 'required|unique:projectlistes,reference',
        'add_name' => 'required',
    ]);
        $project = new projectliste();
        $project->name= $Request->add_name;
        $project->reference= $Request->add_reference;
        $project->ouvrage= $Request->ouvrage;
        $project->building_id = $Request->building_id;
        $project ->save();
        session()->flash('success','votre projet a été enregitrer avec succé');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($build)
    {
        $building=building_type::find($build);
        $project= projectliste::where('building_id', $build)->get();
         
        return $this->view('industrial.borderaux.project.index',compact('project','building'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request, $id)
    {
         $Request->validate([
        'reference' => 'required|unique:projectlistes',
        'name' => 'required',
         ]);

        $project = projectliste ::findOrFail($Request->project_id);
        $project->name= $Request->input('name');
        $project->reference= $Request->input('reference');
        $project->ouvrage= $Request->input('ouvrage');

        $project->save();

        return back();
       
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project=projectliste::find($id);
        $project->delete();
        return back();
    }

    public function storelot(Request $Request)
    {
        $lot=lot::find($Request->lot);
       
        $project_lot = new projectlot();
        $project_lot->lot= $lot->lot_name;
        $project_lot->lot_id= $lot->id;
        $project_lot->project_id = $Request->project_id;
        $project_lot->save();
        $building_id=projectliste::find($Request->project_id);
        
       
        if ($Request->lot == 1) 
        {
            return Redirect::to('/admin/industrial/project/cfo/'.$building_id->building_id.'-'.$building_id->name.'/input');
        }

        return Redirect::to('/admin/industrial/project/bord/'.$Request->lot.'/'.$Request->project_id);
    }
    public function showCFOinput($building_id,$project)
    {
        $clause=commun_clause::where('lot_id',1)->where('building_id',$building_id)->where('with_var',1)->get();
        $project_name=$project;
        
        return $this->view('industrial.CPTP.partials.input_select',compact('clause','project_name'));
    }
    public function saveCFOinput(Request $Request)
    {
          
           $data=$Request->all();
        $lot=projectlot::latest('id')->first();
        $lot->cptp_input=json_encode($data);
        $lot->save();
        $lot_id=$lot->lot_id;
        $project_id=$lot->project_id;
        return Redirect::to('/admin/industrial/project/bord/'.$lot_id.'/'.$project_id);
    }



     public function showlot($projet)
    {
        $project=projectliste::find($projet);
        $projectlot= projectlot::where('project_id', $projet)->get();
        $lot_list=lot::all();
         
        return $this->view('industrial.borderaux.project.projectcontent.lot',compact('project','projectlot','lot_list'));
    }

     public function BORD_list ($lot , $project)
     {  
        $articles=article_bord::where('lot_id',$lot)->get();
        $category=categoryequip::all();
        $sousequip = sousequipement::all();
        $project_name=projectliste::find($project);
        $lotcontent=projectlot::where('project_id',$project)->where('lot_id',$lot)->first();
        $lot_name=lot::find($lot);
        
        if (!empty($lotcontent->lotcontent)) {
        $input=$lotcontent->lotcontent; 
        $data= json_decode($input);

        if (isset($data->article)) { $article_data=$data->article; }
        if (isset( $data->equipment)){$equip_data =(array) $data->equipment;}
        else {$equip_data =[];}
        if (isset( $data->sousequipment)){$sousequip_data = (array) $data->sousequipment;}
        else {$sousequip_data =[];}
        
        if (isset($data->sousquantite)) {$sousquan_data= (array) $data->sousquantite;}
        else{$sousquan_data=NULL;}

        if (isset($data->quantite)) {$quantite_data= (array) $data->quantite;}
        else{$quantite_data=NULL;}

        if (isset($data->four)) {$four_data= (array) $data->four;}
        else{$four_data=NULL;}
        if (isset($data->pose)){$pose_data=(array)$data->pose;}
        else{$pose_data=NULL;} 
        if (isset($data->sousfour)){$sousfour_data=(array) $data->sousfour;}
        else{$sousfour_data=NULL;}
        if (isset($data->souspose)){$souspose_data=(array) $data->souspose;}
        else{$souspose_data=NULL;}
        if (isset($data->spcial)){$spcial_data=(array) $data->spcial;}
        else{$spcial_data=NULL;}
        if (isset($data->souspcial)){$souspcial_data=(array) $data->souspcial;}
        else{$souspcial_data=NULL;}
        if (isset($data->pm)){$pm_data=(array) $data->pm;}
        else{$pm_data=NULL;}
        if (isset($data->souspm)){$souspm_data=(array) $data->souspm;}
        else{$souspm_data=NULL;}

        return $this->view('industrial.borderaux.project.projectcontent.borderaux_list',compact('project_name','lot_name','sousequip','category','sousquan_data','sousequip_data','equip_data','article_data','quantite_data','pose_data','four_data','souspose_data','sousfour_data','spcial_data','souspcial_data','pm_data','souspm_data','articles'));  
        }
        else 
        {
            return $this->view('industrial.borderaux.project.projectcontent.borderaux_list',compact('project_name','lot_name','sousequip','category','articles'));
        }
    
        
        
     } 


      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

}
