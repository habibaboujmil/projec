<?php

namespace App\Http\Controllers\admin\industrial;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\categoryequip;

class categoryequipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new categoryequip();
        if (isset($request->article_id)){$category->article_id= $request->article_id;}
        if (isset($request->equip_id)) {$category->equip_id= $request->equip_id;}
        $category->category_title= $request->category_title;
        $category->save();
         return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request)
    {
        $category = categoryequip::findOrFail($Request->categ_id);
        $category->category_title= $Request->input('category_title');
        $category->save();
       
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
         $category=categoryequip::find($id);
         $category->delete();
         return $this->view('industrial.borderaux.equipments.category.category_equip_list');
    }
}
