<?php

namespace App\Http\Controllers\admin\projectsetting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdminController;
use App\Models\lot;

class lotController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lot_list=lot::all();
        return $this->view('industrial/projectsetting/lot_list',compact('lot_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,['lot_id' => 'required|min:1',]);
        
        foreach ($request->lot_id as $id)
        {
            
            $lot =lot::findOrFail($id);
            $lot->lot_name= $request->lot_name[$id][0];
            $lot->tot_name= $request->tot_name[$id][0];
            $lot->save();
        }
        
        notify()->success('Success!', 'le changement à été bien enregistré.');
       
        return back();
    }

    
}
