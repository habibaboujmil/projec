<?php

namespace App\Http\Controllers\admin\projectSetting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdminController;
use App\Models\specification;

class specificationController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $specification=specification::find(1);
        return $this->view('industrial/projectsetting/specification',compact('specification'));

    }

    public function update(Request $request)
    {
       
        $specification = specification::findOrFail(1);
        $specification->title= $request->input('title');
        $specification->content= $request->input('content');
        
        $specification->save();;
        notify()->success('Success!', 'le changement à été bien enregistré.');
        return back();
    }

}
