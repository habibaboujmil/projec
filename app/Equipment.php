<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipment extends Model
{
	protected $guarded = [];
    use SoftDeletes;

    public function article()
    {
        return $this->belongsTo( article_bord::class);
    }

     public function sousequipement()
    {
        return $this->hasMany(models\sousequipement::class, 'equi_id');
    }


    public static function equip_of_category($category_id) 
    {
       $equip =Equipment::where('category_id', $category_id)->get();

      return $equip;
    }
     public static function equip_of_article($article_id) 
    {
      $categ_null=Equipment::where('article_id', $article_id)->whereNull('category_id')->get();
      $with_categ=Equipment::where('article_id', $article_id)->whereNotNull('category_id')->get();
      $unique_categ=$with_categ->unique('category_id');
      $equi=(object) $unique_categ;
      $equip = $categ_null->merge($equi)->sortBy('id');
      return $equip;
    }
}
