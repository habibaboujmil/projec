<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article_bord extends Model
{
	protected $guarded = [];
    use SoftDeletes;

    static public $rules = [
        'article_title' => 'required',
        
    ];


    public function equipments()
    {
        return $this->hasMany(Equipment::class, 'article_id');
    }

     public function lot()
    {
        return $this->belongsTo(lot::class);
    }

     public function types()
    {
        return $this->belongsToMany('app\models\building_type');
    }
}
