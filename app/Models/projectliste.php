<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class projectliste extends Model
{
	use SoftDeletes;

     public function articles()
    {
        return $this->belongsTo('app\models\projectliste');
    }

    //project has many lots
    public function lots()
    {
        return $this->belongsTo('app\models\projectots');
    }

}
