<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class lot extends Model
{
	use SoftDeletes;
	
    public function articles()
    {
        return $this->hasMany(article_bord::class);
    }
}
