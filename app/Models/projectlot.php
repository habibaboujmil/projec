<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class projectlot extends Model
{

    use SoftDeletes;
    public function projects()
    {
        return $this->belongsTo('app\models\projectliste');
    }
}
