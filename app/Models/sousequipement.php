<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class sousequipement extends Model
{
    use SoftDeletes;

    public function equipment()
    {
        return $this->belongsTo( equipment::class);
    }
    public static function sous_equip($equip_id) 
    {
      $categ_null=sousequipement::where('equi_id', $equip_id)->whereNull('category_id')->get();
      $with_categ=sousequipement::where('equi_id', $equip_id)->whereNotNull('category_id')->get();
      $unique_categs=$with_categ->unique('category_id');
      $unique_categ= $unique_categs->values()->all();
      $sous=(object) $unique_categ;
      $sousequip = $categ_null->merge($sous)->sortBy('id');
      return $sousequip;
    }

    public static function sousequip_of_category($category_id) 
    {
      $sousequip=sousequipement::where('category_id', $category_id)->get();
      return $sousequip;
    }
}
