<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class commun_clause extends Model
{
    use SoftDeletes;


    public static function child_clause($parent_id) 
    {
       $child =commun_clause::where('parent_id', $parent_id)->get();

      return $child;
    }
    
}
