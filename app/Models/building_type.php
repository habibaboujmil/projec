<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class building_type extends Model
{
     public function articles()
    {
        return $this->belongsToMany('app\article_bord');
    }

     public function projects()
    {
        return $this->hasMany('app\models\projects');
    }

}
