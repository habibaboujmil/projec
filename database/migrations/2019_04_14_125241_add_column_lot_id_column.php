->onDelete('restrict')<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLotIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article_bords', function (Blueprint $table) {
             $table-> integer ('lot_id')-> unsigned()->after('id');
            $table-> foreign ('lot_id')-> references('id')->on('lots')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_bords', function (Blueprint $table) {
             $table-> dropforeign ('lot_id');
            $table-> dropColumn ('lot_id');
        });
    }
}

