<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEquiId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sousequipements', function (Blueprint $table) {
            $table-> integer ('equi_id')-> unsigned()->after('id');
            $table-> foreign ('equi_id')-> references('id')->on('equipment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sousequipements', function (Blueprint $table) {
            $table-> dropforeign ('equi_id');
            $table-> dropColumn ('equi_id');
        });
    }
}
