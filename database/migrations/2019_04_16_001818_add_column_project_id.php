<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnProjectId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projectlots', function (Blueprint $table) {
            $table-> integer ('project_id')-> unsigned()->after('id');
            $table-> foreign ('project_id')-> references('id')->on('projectlistes')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projectlots', function (Blueprint $table) {
            $table-> dropforeign ('project_id');
            $table-> dropColumn ('project_id');
        });
    }
}
