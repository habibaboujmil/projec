<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBuildingIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projectlistes', function (Blueprint $table) {
             $table-> integer ('building_id')-> unsigned()->after('id');
            $table-> foreign ('building_id')-> references('id')->on('building_types')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projectlistes', function (Blueprint $table) {
            $table-> dropforeign ('building_id');
            $table-> dropColumn ('building_id');
        });
    }
}



