<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSousequipementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sousequipements', function (Blueprint $table) {
            $table->increments('id');
            $table->text('app_display')->nullable();
            $table->text('sousequi_title')->nullable();
            $table->text('sousequi_description')->nullable();
            $table->json('sousequi_clause')->nullable();
            $table->string('calibre')->nullable();
            $table->string('sousequi_unit_abre')->nullable();
            $table->string('sousequi_unit')->nullable();
            $table->float('four_price')->nullable();
            $table->float('pose_price')->nullable();
            $table->integer('duplicate')->nullable();
            $table->string('descrip')->nullable();
            $table->integer('category_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sousequipements');
    }
}
