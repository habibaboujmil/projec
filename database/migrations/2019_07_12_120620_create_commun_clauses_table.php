<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunClausesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commun_clauses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('building_id')->nullable();
            $table->integer('lot_id')->nullable();
            $table->integer('parent_id')->nullable();
            $table->text('title')->nullable();
            $table->text('clause')->nullable();
            $table->text('clause_file')->nullable();
            $table->integer('position')->nullable();
            $table->integer('with_var')->nullable();
            $table->integer('select')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commun_clauses');
    }
}
