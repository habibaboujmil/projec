<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesBordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_bords', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('building_type')->nullable();
            $table->text('article_title')->nullable();
            $table->text('article_description')->nullable();
            $table->json('article_clause')->nullable();
            $table->text('equipment')->nullable();
            $table->integer('level')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_bords');
    }
}
