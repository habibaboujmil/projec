<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projectlots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lot_id');
            $table->string('lot');
            $table-> json('lotcontent')->nullable();
            $table-> json('cptp_input')->nullable();
            $table-> string('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projectlots');
    }
}
