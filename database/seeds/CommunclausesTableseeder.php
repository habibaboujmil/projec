<?php

use Illuminate\Database\Seeder;
use App\Models\commun_clause;

class CommunclausesTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        commun_clause::truncate();
        $csvPath = database_path() . '/seeds/csv/' . 'communclause.csv';
        $items = csv_to_array($csvPath);

        foreach ($items as $key => $item) {
            commun_clause::create([
                'id'                    => $item['id'],
                'building_id'           => $item['building_id'],
                'lot_id'                => $item['lot_id'],
                'parent_id'             => $item['parent_id'],
                'title'                 => $item['title'],
                'clause'                => $item['clause'],
                'clause_file'           => $item['clause_file'],
                'position'              => $item['position'],
                'with_var'              => $item['with_var'],
                
                
            ]);
        }
    }
}
