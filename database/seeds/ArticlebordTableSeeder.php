<?php

use Illuminate\Database\Seeder;
use App\article_bord;

class ArticlebordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        article_bord::truncate();
        $csvPath = database_path() . '/seeds/csv/' . 'article.csv';
        $items = csv_to_array($csvPath);

        foreach ($items as $key => $item) {
            article_bord::create([
                'id'                    => $item['id'],
                
                'lot_id'                => $item['lot_id'],
                'article_title'         => $item['article_title'],
                'article_description'   => $item['article_description'],
                
            ]);
        }
    }
}
