<?php

use Illuminate\Database\Seeder;
use App\models\lot;

class LotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        lot::truncate();
         $lot = lot::create(['lot_name'    => 'Lot CFO', 'tot_name'=> 'lot électricité courant fort']);
         $lot = lot::create(['lot_name'    => 'Detection Incendie',]);
         $lot = lot::create(['lot_name'    => 'Lot Controle accés',]);
         $lot = lot::create(['lot_name'    => 'Detection Intrusion',]);
         $lot = lot::create(['lot_name'    => 'Lot CCTV',]);
         $lot = lot::create(['lot_name'    => 'Lot VDI',]);
         $lot = lot::create(['lot_name'    => 'Lot Sonorisation',]);
         $lot = lot::create(['lot_name'    => 'Lot Télédistrubition',]);
         $lot = lot::create(['lot_name'    => 'Lot Domotique',]);
         $lot = lot::create(['lot_name'    => 'Lot Ascensseur',]);
    }
}
