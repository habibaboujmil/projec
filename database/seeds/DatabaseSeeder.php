<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // pages, news, blog, albums
        \App\Models\Photo::truncate();

        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);

        $this->call(BannerTableSeeder::class);

        $this->call(PageTableSeeder::class);
        $this->call(NavigationAdminTableSeeder::class);


         $this->call(LotTableSeeder::class);
         
          $this->call(BuildingTypeTableSeeder::class);
    }
}
