<?php

use Illuminate\Database\Seeder;
use App\Models\specification;

class SpecificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        specification::truncate();
        $specification = specification::create(['title'    => 'SPECIFICATIONS GENERALES RELATIVES AUX PRIX UNITAIRES', 'content'=> 'Tous les travaux décrits ci après devront être conformes aux normes et règlements en vigueur']);
    }
}
