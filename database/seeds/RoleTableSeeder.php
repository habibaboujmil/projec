<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        Role::truncate();

        // basic website only role
        Role::create([
            'icon'    => 'desktop',
            'name'    => 'Website',
            'slug'    => '/',
            'keyword' => 'website',
        ]);

        // base admin role (to be able to log into /admin)
    Role::create([
        'icon'    => 'user-secret',
        'name'    => 'Base Admin',
        'slug'    => '/admin',
        'keyword' => 'base_admin',
    ]);

    // super
    Role::create([
        'icon'    => 'user-secret',
        'name'    => 'Admin',
        'slug'    => '/admin',
        'keyword' => 'admin',
    ]);

    // developer
    Role::create([
        'icon'    => 'universal-access',
        'name'    => 'Developer',
        'slug'    => '/admin',
        'keyword' => 'developer',
    ]);



    }
}
