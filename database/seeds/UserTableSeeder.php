<?php

use App\User;
use Carbon\Carbon;
use App\Models\Role;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run(Faker\Generator $faker)
    {
        User::truncate();
        //DB::delete('TRUNCATE role_user'); //Unnecessary in Laravel 5.6

        //-------------------------------------------------
        // Ben-Piet
        //-------------------------------------------------
        $user = User::create([
            'firstname'    => 'Mohamed',
            'lastname'     => 'Boujmil',
            'cellphone'    => '123456789',
            'email'        => 'boujmil.mohamed91@gmail.com',
            'gender'       => 'male',
            'password'     => bcrypt('boujmil'),
            'confirmed_at' => Carbon::now()
        ]);

        $this->addRolesToUser($user, 2);

        //-------------------------------------------------
        // GitHub
        //-------------------------------------------------
        $user = User::create([
            'firstname'    => 'Habiba',
            'lastname'     => 'Boujmil',
            'cellphone'    => '123456789',
            'email'        => 'boujmil.habiba95@gmail.com',
            'gender'       => 'ninja',
            'password'     => bcrypt('habiba'),
            'confirmed_at' => Carbon::now()
        ]);

        $this->addRolesToUser($user, 3);

        //-------------------------------------------------
        // Default Admin
        //-------------------------------------------------
        $user = User::create([
            'firstname'    => 'Admin',
            'lastname'     => 'Admin',
            'cellphone'    => '123456789',
            'email'        => 'admin@gmail.com',
            'gender'       => 'male',
            'password'     => bcrypt('admin'),
            'confirmed_at' => Carbon::now()
        ]);

        $this->addRolesToUser($user, 4);

        // dummy users
        /*for ($i = 0; $i < 5; $i++) {
            $user = User::create([
                'firstname'    => $faker->firstName,
                'lastname'     => $faker->lastName,
                'cellphone'    => $faker->phoneNumber,
                'email'        => $faker->email,
                'gender'       => $faker->randomElement(['male', 'female']),
                'password'     => bcrypt('secret'),
                'confirmed_at' => Carbon::now()
            ]);

            $user->syncRoles([
                \App\Models\Role::$WEBSITE,
            ]);
        }*/
    }

    /**
     * Add all the roles to the user
     * @param $user
     */
    private function addRolesToUser($user, $roleId)
    {
        // only 2 - to 5 are needed
        $roles = Role::whereBetween('id', [2,$roleId])
            ->pluck('keyword', 'id')
            ->values();

        $user->syncRoles($roles);
    }
}
