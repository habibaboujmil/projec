<?php

use Illuminate\Database\Seeder;
use App\Models\building_type;

class BuildingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        building_type::truncate();
        $csvPath = database_path() . '/seeds/csv/' . 'buildingtype.csv';
        $items = csv_to_array($csvPath);

        foreach ($items as $key => $item) {
            building_type::create([
                'id'                    => $item['id'],
                'type'                => $item['type'],
            ]);
        }
    }
}
